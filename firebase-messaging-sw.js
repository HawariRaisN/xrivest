importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyB4Ddoz4G4P9KnjLrSI8XF5R0vqm3M4qB0",
    authDomain: "rivest-4507.firebaseapp.com",
    databaseURL: "https://rivest-4507.firebaseio.com",
    projectId: "rivest-4507",
    storageBucket: "rivest-4507.appspot.com",
    messagingSenderId: "1062631027"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: 'default'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});


