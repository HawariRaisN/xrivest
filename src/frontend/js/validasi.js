$(function () {
    $('#sign_up').validate({
        rules: {
            'terms': {
                required: true
            },
            'c_password': {
                equalTo: '[name="password"]'
            },
            no_hp:{
                required:true,
                number:true,
                minlength:12,
                remote:{
                    url:url_cek_no_hp,
                    type:"POST",
                    data:{
                        id_ktp: function()
                        {
                            return $('#sign_up :input[name="no_hp"]').val();
                        }
                    }
                }
            },
            email:{
                email:true,
                required:true,
                remote:{
                    url:url_cek_email,
                    type:"POST",
                    data:{
                        email: function()
                        {
                            return $('#sign_up :input[name="email"]').val();
                        }
                    }
                }
            }
            ,
            username:{
                required:true,
                remote:{
                    url:url_cek_username,
                    type:"POST",
                    data:{
                        email: function()
                        {
                            return $('#sign_up :input[name="username"]').val();
                        }
                    }
                }
            }
        },
        messages:{
            email:{
                required: "Please enter your email address.",
                email: "Format email salah",
                remote: jQuery.validator.format("{0} Email sudah ada")
            },
            username:{
                required: "Please enter your username.",
                remote: jQuery.validator.format("{0} sudah ada")
            },
            no_hp:{
                required: "Please enter your number phone",
                number: "Data harus number",
                remote: jQuery.validator.format("{0} sudah ada"),
                minlength:"Format No HP salah"
            }
        },
        highlight: function (input) {
            console.log(input);
            $(input).parents('.form-group').addClass('text-danger');
        },
        unhighlight: function (input) {
            $(input).parents('.form-group').removeClass('text-danger');
        },
        errorPlacement: function (error, element) {
            // $(element).parents('.input-group').append(error);
            $(element).parents('.form-group').append(error);
        }
    });
});