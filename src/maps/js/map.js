// Initialize Firebase
var config = {
    apiKey: "AIzaSyB4Ddoz4G4P9KnjLrSI8XF5R0vqm3M4qB0",
    authDomain: "rivest-4507.firebaseapp.com",
    databaseURL: "https://rivest-4507.firebaseio.com",
    projectId: "rivest-4507",
    storageBucket: "rivest-4507.appspot.com",
    messagingSenderId: "1062631027"
};

firebase.initializeApp(config);
var db = firebase.database();
var accidents = db.ref().child('accidents');


var map, markers = [], bounds;
var button = document.getElementById('kirim');
var lat = document.getElementById('lat');
var lng = document.getElementById('lng');

// button.addEventListener('click', kirimData);

// function kirimData(e) {
//     e.preventDefault();

//     accidents.push({
//         id_pengguna:2,
//         lat:lat.value,
//         lng:lng.value,
//         status:'Belum Ditangani',
//         tanggal:'2018-03-27'
//     });
// }


function getAwal(){
   
    
    accidents.once('value', function(snap){

        // console.log(markers);
        
        
        data = snap.val();

        
            for(k in data){

                file = data[k];

                if(file.status == "Belum Ditangani"){
                    var latlng = new google.maps.LatLng(parseFloat(file.lat), parseFloat(file.lng));
                    
                    var infowindowContent = 'Waktu kejadian :' + file.tanggal;

                    bounds.extend(latlng);

                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                    });
                    
                    openInfo(marker, infowindowContent);

                    markers.push(marker);

                    map.fitBounds(bounds);
                    
                }

                

            }

            

           
             
    });
}



function initMap() {
    
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -6.91746390, lng: 107.61912280},
        zoom: 13,
        mapTypeControl: false
    });

  
    bounds = new google.maps.LatLngBounds();

    accidents.on('child_added', addMarker);

    accidents.on('child_changed', changeMarker);

    accidents.once('value', function(snap){
        
        // if(markers.length > 0){
        //     reset_map();
        // }

        console.log('Awal');
        
        
        data = snap.val();

        
            for(k in data){

                file = data[k];

                if(file.status == "Belum Ditangani"){
                    var latlng = new google.maps.LatLng(parseFloat(file.lat), parseFloat(file.lng));
                    
                    var infowindowContent = 'Waktu kejadian :' + file.tanggal;

                    bounds.extend(latlng);

                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                    
                    openInfo(marker, infowindowContent);

                    map.fitBounds(bounds);
                    
                }

            }

            
             
    });
        
}

function changeMarker(snap){

    reset_map();
}

function addMarker(snap) {

        var data = snap.val();
            
        if(data.status == "Belum Ditangani"){
            var latlng = new google.maps.LatLng(parseFloat(data.lat), parseFloat(data.lng));
            
            var infowindowContent = 'Waktu kejadian :' + data.tanggal;

            bounds.extend(latlng);

            var marker = new google.maps.Marker({
                position: latlng,
                    map: map,
                    animation: google.maps.Animation.DROP
                });
                
            openInfo(marker, infowindowContent);

             markers.push(marker);

             map.fitBounds(bounds);
        }
        
    
}

function openInfo(mark, infowindowContent){
    
    var infoWindow = new google.maps.InfoWindow({
        content: infowindowContent, 
        maxwidth : 400
    });
    

    google.maps.event.addListener(mark, 'click', function() {
        infoWindow.open(map, mark);
    })
}

function reset_map() {
    

    for(i in markers){

        markers[i].setVisible(false);
    }

    markers =[];

    getAwal();
    
}

