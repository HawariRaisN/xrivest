<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Rawan extends MY_Controller {

  protected $access = array('Admin', 'Police', 'JasaRaharja');

  
  public function __construct()
  {
      parent::__construct();
      $this->load->model('model_rawan'); 
  }
  
  public function index()
  {
       
      $data = array(
          's1' => 'Red Zone',
          'konten' => $this->model_rawan->getRawan(),
          'poly' => $this->model_rawan->getPoly(),
          'start' => 0
      );
      $this->load->view('cover/header');
      $this->load->view('redzone/index', $data);
      $this->load->view('cover/footer2');
      $this->load->view('redzone/js');
  }

  function add()
    {
        $data = array(
            's1' => 'Red Zone',
        );
        $this->load->view('cover/header');
        $this->load->view('redzone/add_redzone', $data);
        $this->load->view('cover/footer2');
        $this->load->view('redzone/js_add');
    }
    
    function action_add()
    {
       $data = $this->input->post(null,true);
       $res = $this->model_rawan->proses_input_data($data);
       if ($res>=1) {
           redirect('rz');           
       } else {
            redirect('rz');
       }                  
    }

    function edit_rawan($id_rawan){
        $id_rawan = $this->uri->segment(3);
        $where = array('id_rawan' => $id_rawan);
        $data = array(
            's1' => 'Red Zone',
            "konten" => $this->model_rawan->get_data_where($where)
        );
        $this->load->view('cover/header');
        $this->load->view('redzone/update_redzone', $data);
        $this->load->view('cover/footer2');
        $this->load->view('redzone/js_add');
    }

    function proses_edit_rawan(){
        $id_rawan= $this->input->post('id_rawan');
        $data = $this->input->post(null, true);
        unset($data['id_rawan']);
        $res = $this->model_rawan->proses_edit_data($id_rawan,$data);
        if($res>=1){
            redirect('rz');
        }
        else{
            redirect('rz');
        }
    }

    function delete_rawan()
    {
        
        $id_rawan = $this->uri->segment(3);
        $data = array( 'id_rawan' => $id_rawan );
        $res = $this->model_rawan->proses_delete_data($data);
        redirect('rz');
    }

}

/* End of file Rawan.php */
