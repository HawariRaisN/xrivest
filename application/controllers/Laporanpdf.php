<?php
Class Laporanpdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('fpdf_gen');
    }
    
    function index($data){

        $expload = explode("~",urldecode($data));
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        $pdf->Image('https://cdn.sindonews.net/dyn/620/content/2015/01/12/16/949324/tantangan-kapolri-baru-mvL.jpg',10,7,35);
        // mencetak string 
        $pdf->Cell(200,10,'SURAT KETERANGAN KECELAKAAN LALU LINTAS',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'(BAP)',0,1,'C');

        $pdf->Cell(190,8,'','B',1,'L');
        $pdf->Cell(190,1,'','B',0,'L');

        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->Cell(200,10,'Nama      :  Kikit Teguh Waluyo ',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->cell(200,10,'Jabatan   : Korlap',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->cell(200,10,'Menerangkan dengan sebenarnya bahwa telah terjadi kecelakaan yang menimpa seseorang atas nama :',0,1);        

        $pdf->SetFont('Arial','',10);
        $pdf->Cell(200,10,'Nama      :  '.$expload[0].'',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->Cell(200,10,'Alamat    : '.$expload[1].'',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->Cell(200,10,'Jenis Kelamin :  '.$expload[2].'',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->Cell(200,10,'Tempat    :  '.$expload[3].'',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->cell(200,10,'Adapun uraian kejadiannya adalah sebagai berikut :',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->cell(200,10,'1. Kecelakaan terjadi pada '.$expload[4].'',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->cell(200,10,'2. Lokasi kejadiannya '.$expload[5].'',0,1);
        
        $pdf->SetFont('Arial','',10);
        $pdf->cell(200,10,'3. Kronologi '.$expload[6].'',0,1);

        $pdf->SetFont('Arial','',10);
        $pdf->cell(200,10,'Demikian Surat ini dibuat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.',0,1);

        $pdf->Cell(190,8,'','B',1,'L');
        $pdf->Cell(190,1,'','B',0,'L');
        $pdf->Output();
    }
}