<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Frontend extends CI_Controller{

	function __Construct(){  
    parent::__Construct();  
    $this->load->helper(array('form', 'url'));  
    $this->load->library(array('session', 'form_validation', 'email'));   
    $this->load->database();  
    $this->load->model('user_model');  
    }

	public function panggil()
	{
		$data_= array(
			'link' => site_url("regis/verification/afadad"),
			'nama' =>  'Irfan'
		);
		$this->load->view('user/tampilan_email',$data_);
	}
  	public function index()
	{
		$this->load->view('user/index');
	}

	public function regis_tenant()
	{
		$this->load->view('user/register_tenant');
	}
    
    public function cekEmail()
	{
		$email = $this->input->post('email');
		$query = $this->db->get_where('root', array('email' => $email));

		if($query->num_rows() > 0){
			echo "false";
		}else{
			echo "true";
		}

	}

	public function cekNoHp()
	{
		$no_hp = $this->input->post('no_hp');
		$query = $this->db->get_where('root', array('no_hp' => $no_hp));

		if($query->num_rows() > 0){
			echo "false";
		}else{
			echo "true";
		}

	}

	public function cekUsername()
	{
		$username = $this->input->post('username');
		$query = $this->db->get_where('root', array('username' => $username));

		if($query->num_rows() > 0){
			echo "false";
		}else{
			echo "true";
		}

	}
	
	public function store_registrasi()  
	{
		$username		 = $this->input->post('username');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$bidang			 = $this->input->post('bidang');
		$nama 			 = $this->input->post('nama');
		$alamat			 = $this->input->post('alamat');
		$no_hp 			 = $this->input->post('no_hp');
		$email 			 = $this->input->post('email');
		$password		 = sha1($this->input->post('password'));
		$status			 = 0;

		$data = array(
			'username'		  => $username,
			'role'			  => 'Tennant',
			'nama_perusahaan' => $nama_perusahaan,
			'bidang' 		  => $bidang,
			'nama'			  => $nama,
			'alamat'		  => $alamat,
			'no_hp'			  => $no_hp,
			'email'			  => $email,
			'password'		  => $password,
			'status'		  => $status,
		);

		$id = $this->user_model->add_account($data);

		//enkripsi id
    	$encrypted_id = md5($id)."-1xx".$id;

		$this->load->library('email');

		$data_= array(
			'link' => site_url("regis/verification/$encrypted_id"),
			'nama' =>  $nama
		);

		$e = $this->load->view('user/tampilan_email',$data_,true);

		// Get full html:
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
		    ->from('myrivest@gmail.com','Myrivest.net')
		    ->to($email)
		    ->subject("Aktivasi akun")
		    ->message($e)
		    ->send();

		// var_dump($result);
		// echo '<br />';
		// echo $this->email->print_debugger();
		    if ($result) {
				$this->session->set_flashdata('berhasil_kirim', 'berhasil');
		    	return redirect('regis_tenant');
		    }
		    else
		    {
		    	echo "gagal";
		    }

		exit;
	}

	public function verification($key)
	{
		$this->user_model->changeActiveState($key);
		
		$this->session->set_flashdata('berhasil','akun anda sudah aktif');

		redirect('rivest-login');
		
	}
  }
