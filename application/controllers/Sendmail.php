<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendmail extends CI_Controller {

	public function index()
	{
				$this->load->library('email');

		$subject = 'This is a test';
		$message = '<p>This message has been sent for testing purposes.</p>';

		// Get full html:
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
		    <title>' . html_escape($subject) . '</title>
		    <style type="text/css">
		        body {
		            font-family: Arial, Verdana, Helvetica, sans-serif;
		            font-size: 16px;
		        }
		    </style>
		</head>
		<body>
		' . $message . '
		</body>
		</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
		    ->from('aditiyafadillah@email.unikom.ac.id')
		    ->reply_to('aditiyafadillah@email.unikom.ac.id')    // Optional, an account where a human being reads.
		    ->to('wgnagun@gmail.com')
		    ->subject($subject)
		    ->message($body)
		    ->send();

		// var_dump($result);
		// echo '<br />';
		// echo $this->email->print_debugger();
		    if ($result) {
		    	echo "email berhasil terkirim";
		    }
		    else
		    {
		    	echo "gagal";
		    }

		exit;
	}

}

/* End of file Sendmail.php */
/* Location: ./application/controllers/Sendmail.php */