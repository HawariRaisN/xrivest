<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends MY_Controller {

    protected $access = array('Admin', 'Police', 'JasaRaharja');

    public function index()
    {
        $data = array(
            's1' => 'Rivest Users',
        );
        $this->load->view('cover/header');
        $this->load->view('pengguna/index', $data);
        $this->load->view('cover/footer2');
        $this->load->view('pengguna/js');
        
    }

}

/* End of file Pengguna.php */
