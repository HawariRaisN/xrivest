<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kecelakaan extends MY_Controller {

    protected $access = array('Admin', 'Police', 'JasaRaharja', 'Dishub');
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_pos');
        $this->load->model('model_rawan'); 
    }
    

    public function index()
    {
       
        $data = array(
            's1' => 'Rivest Monitoring',
            'rawan' => $this->model_rawan->getRawan(),
            'pos' => $this->model_pos->getPos()
        );
        $this->load->view('cover/header');
        $this->load->view('kecelakaan/index', $data);
        $this->load->view('cover/footer2');
        $this->load->view('kecelakaan/js');
        
    }

    public function rute()
    {
       
        $data = array(
            's1' => 'Rute Tercepat',
        );
        $this->load->view('cover/header');
        $this->load->view('kecelakaan/dijkstra', $data);
        $this->load->view('cover/footer2');
        $this->load->view('kecelakaan/js_dijkstra');
        
    }

    public function accident()
    {
        $data = array(
            's1' => 'Data Of Accidents',
        );
        $this->load->view('cover/header');
        $this->load->view('kecelakaan/accident', $data);
        $this->load->view('cover/footer2');
        $this->load->view('kecelakaan/js_accident');
        
    }

    public function decission()
    {
        $data = array(
            's1' => 'Data Of Decission',
        );
        $this->load->view('cover/header');
        $this->load->view('kecelakaan/decission', $data);
        $this->load->view('cover/footer2');
        $this->load->view('kecelakaan/js_decission');
        
    }

    public function jsrhrj()
    {
        $data = array(
            's1' => 'List Of Approve Polices Accident Letter',
        );
        $this->load->view('cover/header');
        $this->load->view('kecelakaan/jsrhrj', $data);
        $this->load->view('cover/footer2');
        $this->load->view('kecelakaan/js_jsrhrj');
        
    }

}

/* End of file Kecelakaan.php */
