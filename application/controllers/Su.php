<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Su extends MY_Controller {

    protected $access = array('Admin', 'Police', 'JasaRaharja');

    
    public function __construct()
    {
      parent::__construct();
      $this->load->model('Su_model', 'rm');
    }
    

    public function index()
  {
      
      $data = $this->rm->get_all();
      $kirim = array(
        'data_root' => $data ,
        's1' => 'List Of Super Account',
        'start' => 0
      );  
      $this->load->view('cover/header');
      $this->load->view('root/index', $kirim);
      $this->load->view('cover/footer');  
  }

  public function add()
  {
    $data = array(
      'nama' => $this->input->post('nama'),
      'role' => $this->input->post('role'),
      'username' => $this->input->post('username'),
      'password' => sha1($this->input->post('password')),
    );
    $this->rm->insert($data);
    $this->session->set_flashdata('message', 'Create Account Success');
    redirect(site_url('su'));
  }

//   function edit_su($id_su){
//     $id_su = $this->uri->segment(3);
//     $where = array('id' => $id_su);
//     $data = array(
//         's1' => 'Red Zone',
//         "konten" => $this->rm->get_data_where($where)
//     );
//     $this->load->view('cover/header');
//     $this->load->view('redzone/update_redzone', $data);
//     $this->load->view('cover/footer2');
//     $this->load->view('redzone/js_add');
// }

// function proses_edit_su(){
//     $id_su= $this->input->post('id_su');
//     $data = $this->input->post(null, true);
//     unset($data['id_su']);
//     $res = $this->rm->proses_edit_data($id_su,$data);
//     if($res>=1){
//         redirect('rz');
//     }
//     else{
//         redirect('rz');
//     }
// }

function delete_su()
{
    $id_su = $this->uri->segment(3);
    $data = array('id' => $id_su );
    $res = $this->rm->proses_delete_data($data);
    $this->session->set_flashdata('message', 'Delete Account Success');
    redirect('su');
}

}

/* End of file Alat.php */
