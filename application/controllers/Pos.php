<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends MY_Controller {

  protected $access = array('Admin', 'Police', 'JasaRaharja');
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_pos'); 
  }
  
  
  public function index()
  {
    
    $data = array(
        's1' => 'Polices Station',
        'konten' => $this->model_pos->getPos(),
        'start' => 0
    );
    $this->load->view('cover/header');
    $this->load->view('police/index', $data);
    $this->load->view('cover/footer2');
    $this->load->view('police/js');
  }

  public function add()
  {
    $data = array(
        's1' => 'Polices Station',
    );
    
    $this->load->view('cover/header');
    $this->load->view('police/add_police', $data);
    $this->load->view('cover/footer2');
    $this->load->view('police/js_add');

  }

  public function action_add()
  {
    $data = $this->input->post(null,true);
    $res = $this->model_pos->proses_input_data($data);
    if ($res>=1) {
        redirect('ps');           
    } else {
         redirect('ps');
    }

  }

  function update($id_pos){
    $id_pos = $this->uri->segment(3); 
    $where = array('id_pos' => $id_pos);
    $data = array(
        's1' => 'Polices Station',
        'konten' => $this->model_pos->get_data_where($where)
    );
    $this->load->view('cover/header');
    $this->load->view('police/update_police', $data);
    $this->load->view('cover/footer2');
    $this->load->view('police/js_up'); 
}

function action_update(){
    $id_pos= $this->input->post('id_pos');
    $data = $this->input->post(null, true);
    unset($data['id_pos']);
    $res = $this->model_pos->proses_edit_data($id_pos,$data);
    if($res>=1){
        redirect('ps');
    }
    else{
        redirect('ps');
    }
}

function delete_pos($id_pos)
{
    $id_pos = $this->uri->segment(3);
    $data = array( 'id_pos' => $id_pos );
    $res = $this->model_pos->proses_delete_data($data);
    redirect('ps');
}

}

/* End of file Pos.php */
