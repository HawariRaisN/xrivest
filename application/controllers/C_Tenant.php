<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Tenant extends CI_Controller {

	public function __construct()
    {
      parent::__construct();
      $this->load->model('user_model');
    }

	public function index()
	{
		$data = array(
	        's1' => 'Tenant',
	        'konten' => $this->user_model->getPos(),
    	);

		$this->load->view('cover/header');
		$this->load->view('Tennant/index', $data);
		$this->load->view('cover/footer2');
        $this->load->view('Tennant/js');

	}

	public function cek_validasi()
	{
		$id = $this->session->userdata('id');
        $query = $this->db->get_where('des_tenant', array('id_tenant' => $id))->row();
		
		if($query->status_tenant == 'trailer'){
			$id = $this->session->userdata('id');
			
			
			$query = $this->db->get_where('des_tenant', array('id_tenant' => $id));
			$get_tanggal = $query->row()->expaired;
			
			if (date('Y-m-d') > $get_tanggal) {
				$this->session->set_flashdata('waktu_habis','Maaf akun anda sudah expaired');
				return redirect('regis');
			}

			$query1 = $this->db->get_where('iklan', array('id_tennant' => $id));


			if($query1->num_rows() >= 3){
				$this->session->set_flashdata('waktu_habis','Upgrade akun mu menjadi premium');
				redirect('regis');
			}
		}
	}

	public function regis_store()
	{
		
		$this->cek_validasi();

		$name = time().'-erg';
 		$config['upload_path']          = './sementara/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 100000;
        $config['max_width']            = 100000;
        $config['max_height']           = 100000;
        $config['file_name']            = $name;

        $data = $this->input->post(null,true);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar'))
        {
		   $this->session->set_flashdata('error_image', $this->upload->display_errors());
           redirect('formAdd');
        }
        else
        {
			$gbr = $this->upload->data();
		

			$config2['image_library'] = 'gd2'; 
			$config2['source_image'] = $this->upload->upload_path.$gbr['file_name'];
			$config2['new_image'] = './uploads/'.$gbr['file_name']; // folder tempat menyimpan hasil resize
		    
            $config2['maintain_ratio'] = TRUE;
			$config2['width'] = 1000; //lebar setelah resize menjadi 100 px
			$config2['height'] = 1000; //lebar setelah resize menjadi 100 px
			
			$this->load->library('image_lib');
			
			$this->image_lib->clear();
			$this->image_lib->initialize($config2);
			$this->image_lib->resize();

			@unlink($this->upload->upload_path.$gbr['file_name']);

            $gambar = base_url().'uploads/'.$gbr['file_name'];
            $data['id_tennant'] = $this->session->userdata('id');
            $data['gambar'] = $gambar;

            $test = $this->user_model->proses_input_data($data);

            if ($test >= 1) {
		        redirect('regis');           
		    } else {
		        redirect('regis');
		    }
        }
	}

	public function formAdd()
	{
		$data = array(
        	's1' => 'Tenant',
    	);
    
	    $this->load->view('cover/header');
	    $this->load->view('Tennant/add_tenant', $data);
	    $this->load->view('cover/footer2');
	    $this->load->view('Tennant/js_add');
	}

	function update($id_iklan)
	{
	    $id_iklan = $this->uri->segment(3); 
	    $where = array('id_iklan' => $id_iklan);

	    $data = array(
	        's1' => 'Polices Station',
	        'konten' => $this->user_model->get_data_where($where)
	    );

	    $this->load->view('cover/header');
	    $this->load->view('Tennant/update_tenant', $data);
	    $this->load->view('cover/footer2');
	    $this->load->view('Tennant/js_up');  
	}

	public function action_update()
	{
	    
	    $name = time().'-erg';
 		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['max_width']            = 10000;
        $config['max_height']           = 10000;
        $config['file_name']            = $name;

	    $id_iklan = $this->input->post('id_iklan');
	    $data = $this->input->post(null, true);

	    if (!empty($_FILES['gambar']['name'])) {
	    	$gambar = $this->user_model->get_gambar($id_iklan);
	    	$expload = explode(base_url(), $gambar['gambar']);
	    	if (file_exists($expload[1])){
	    		unlink($expload[1]);
	    	}

	    	$this->load->library('upload', $config);

	        if ($this->upload->do_upload('gambar'))
	        {
	            
	            $gambar = base_url().'uploads/'.$this->upload->data()['file_name'];
	            $data['gambar'] = $gambar;

	            unset($data['id_iklan']);

	            $test = $this->user_model->proses_edit_data($id_iklan, $data);

	            if ($test >= 1) {
			        redirect('regis');           
			    } else {
			        redirect('regis');
			    }
	        }


	    }else{

	    	unset($data['id_iklan']);
	    
		    $res = $this->user_model->proses_edit_data($id_iklan,$data);
		    if($res>=1){
		        redirect('regis');
		    }
		    else{
		        redirect('regis');
	    	}
	    }

	    
	}

	function delete_tenant($id_iklan)
	{
	    $id_iklan = $this->uri->segment(3);
	    $data = array( 'id_iklan' => $id_iklan );
	    $res = $this->user_model->proses_delete_data($data);
	    redirect('regis');
	}

}

/* End of file C_Tenant.php */
/* Location: ./application/controllers/C_Tenant.php */