<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Alat extends MY_Controller {

    protected $access = array('Admin', 'Police', 'JasaRaharja');

    public function index()
    {
        $data = array(
            's1' => 'Rivest Devices',
        );
        $this->load->view('cover/header');
        $this->load->view('alat/index', $data);
        $this->load->view('cover/footer2');
        $this->load->view('alat/js');
    }

    public function test()
    {
        $this->load->view('test');
    }

    public function tambah_data()
    {
        $this->load->view('cover/header');
        $this->load->view('alat/alat_form');
        $this->load->view('cover/footer2');
        $this->load->view('alat/js');
    }

}

/* End of file Alat.php */
