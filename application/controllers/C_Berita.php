<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Berita extends CI_Controller {

	public function __construct()
    {
      parent::__construct();
      $this->load->model('Model_berita','berita');
    }

	public function index()
	{
		$data = array(
	        's1' => 'Detail Berita',
	        'konten' => $this->berita->getBerita(),
    	);

		$this->load->view('cover/header');
		$this->load->view('berita/index', $data);
		$this->load->view('cover/footer2');
		$this->load->view('Tennant/js');

    }
    
    public function addBerita()
	{
		$data = array(
	        's1' => 'Tambah Berita',
    	);

		$this->load->view('cover/header');
		$this->load->view('berita/tambah_berita', $data);
        $this->load->view('cover/footer2');
        $this->load->view('berita/js');

    }
    
    public function prosesAdd()
    {

		$name = time().'-erg';
 		$config['upload_path']          = './sementara/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100000;
        $config['max_width']            = 10000;
        $config['max_height']           = 10000;
        $config['file_name']            = $name;

        $data = $this->input->post(null,true);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar'))
        {
		   $this->session->set_flashdata('error_image', $this->upload->display_errors());
           redirect('formAdd');
        }
        else
        {
			$gbr = $this->upload->data();

			$config2['image_library'] = 'gd2'; 
			$config2['source_image'] = $this->upload->upload_path.$gbr['file_name'];
			$config2['new_image'] = './uploads/berita/'.$gbr['file_name']; // folder tempat menyimpan hasil resize
			$config2['maintain_ratio'] = TRUE;
			$config2['width'] = 1000; //lebar setelah resize menjadi 100 px
			$config2['height'] = 1000; //lebar setelah resize menjadi 100 px
			$this->load->library('image_lib');
			$this->image_lib->clear();
			$this->image_lib->initialize($config2);
			$this->image_lib->resize();

            @unlink($this->upload->upload_path.$gbr['file_name']);
            
			date_default_timezone_set('Asia/Jakarta');
		
            $gambar = base_url().'uploads/berita/'.$this->upload->data()['file_name'];
            $data['id_root'] = $this->session->userdata('id');
            $data['gambar'] = $gambar;
            
            $string=preg_replace('/[^a-zA-Z0-9 &%|{.}=,?!*()"-_+$@;<>]/', '',  $data['judul_berita']);
            $trim=trim($string);
            $pre_slug=strtolower(str_replace(" ", "-", $trim));
            $data['slug'] = $pre_slug;

            $test = $this->berita->proses_input_data($data);

            if ($test >= 1) {
                $this->session->set_flashdata('success','Data sudah disimpan');
		        redirect('tambah_berita');           
		    } else {
		        redirect('tambah_berita');
		    }
        }
    }

    function updateBerita($id_iklan)
	{ 
	    $where = array('id_berita' => $id_iklan);

	    $data = array(
	        's1' => 'Update Berita',
	        'konten' => $this->berita->get_data_where($where)
	    );

	    $this->load->view('cover/header');
	    $this->load->view('berita/edit_berita', $data);
	    $this->load->view('cover/footer2');
	}

	public function getBerita()
	{
		$data = array(
	        's1' => 'Update Berita',
	        'konten' => $this->berita->get_coyyy()
		);
		
		$this->load->view('berita/portal', $data);
	}

	public function detailBeritaFrontend($slug)
	{
		$where = array('slug' => $slug);
	    $data = array(
	        's1' => 'Detail Berita',
	        'konten' => $this->berita->get_data_where1($where)
	    );

	   $this->load->view('berita/frontend_berita', $data);
	   
	}

	public function detailBerita($id_iklan)
	{
		$where = array('slug' => $id_iklan);

	    $data = array(
	        's1' => 'Detail Berita',
	        'konten' => $this->berita->get_data_where($where)
	    );

	    $this->load->view('cover/header');
	    $this->load->view('berita/detail', $data);
	    $this->load->view('cover/footer2');
	}

	public function action_update()
	{
	    
	    $name = time().'-erg';
 		$config['upload_path']          = './sementara/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['max_width']            = 10000;
        $config['max_height']           = 10000;
        $config['file_name']            = $name;

	    $id_iklan = $this->input->post('id_berita');
	    $data = $this->input->post(null, true);

	    if (!empty($_FILES['gambar']['name'])) {
	    	$gambar = $this->berita->get_gambar($id_iklan);
			$expload = explode(base_url(), $gambar['gambar']);
			
	    	if (file_exists($expload[1])){
	    		unlink($expload[1]);
	    	}

	    	$this->load->library('upload', $config);

	        if ($this->upload->do_upload('gambar'))
	        {
				
				$gbr = $this->upload->data();

				$config2['image_library'] = 'gd2'; 
				$config2['source_image'] = $this->upload->upload_path.$gbr['file_name'];
				$config2['new_image'] = './uploads/berita/'.$gbr['file_name']; // folder tempat menyimpan hasil resize
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 1000; //lebar setelah resize menjadi 100 px
				$config2['height'] = 1000; //lebar setelah resize menjadi 100 px
				$this->load->library('image_lib');
				$this->image_lib->clear();
				$this->image_lib->initialize($config2);
				$this->image_lib->resize();

				@unlink($this->upload->upload_path.$gbr['file_name']);
				
	            $gambar = base_url().'uploads/berita/'.$this->upload->data()['file_name'];
				$data['gambar'] = $gambar;
				
				$string=preg_replace('/[^a-zA-Z0-9 &%|{.}=,?!*()"-_+$@;<>]/', '',  $data['judul_berita']);
				$trim=trim($string);
				$pre_slug=strtolower(str_replace(" ", "-", $trim));
				$data['slug'] = $pre_slug;

	            unset($data['id_berita']);

	            $test = $this->berita->proses_edit_data($id_iklan, $data);

	            if ($test >= 1) {
					$this->session->set_flashdata('success','Data berhasil diperbaharui');
			        redirect('tampil_berita');           
			    } else {
			        redirect('tampil_berita');
			    }
	        }


	    }else{

			unset($data['id_iklan']);
			
			$string=preg_replace('/[^a-zA-Z0-9 &%|{.}=,?!*()"-_+$@;<>]/', '',  $data['judul_berita']);
			$trim=trim($string);
			$pre_slug=strtolower(str_replace(" ", "-", $trim));
			$data['slug'] = $pre_slug;
	    
			$res = $this->berita->proses_edit_data($id_iklan,$data);
			
		    if($res>=1){
		        $this->session->set_flashdata('success','Data berhasil diperbaharui');
			    redirect('tampil_berita');   
		    }
		    else{
		        redirect('tampil_berita');
	    	}
	    }
	}

	public function hapusBerita($id_berita)
	{
		
	    $data = array('id_berita' => $id_berita);
		$res = $this->berita->proses_delete_data($data);
		$this->session->set_flashdata('success','Data berhasil dipedihapus');
	    redirect('tampil_berita');
	}
}

/* End of file C_Tenant.php */
/* Location: ./application/controllers/C_Tenant.php */