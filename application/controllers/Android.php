<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Android extends CI_Controller {

  
    public function __construct()
    {
      parent::__construct();
      $this->load->model('Model_android', 'Am');
      date_default_timezone_set("Asia/Bangkok");
    }
  
    public function getRawan()
    {
      $cari = $this->Am->getRawan();
      echo(json_encode($cari));    
    }

    public function getlokasi()
    {
      $_lat = $this->input->post('lat');
      $_lng = $this->input->post('lng');
      $cari = $this->Am->getLokasi($_lat, $_lng);
      echo(json_encode($cari)); 
    }

    
    public function hitung()
    {

      $_lat = $this->input->post('lat');
      $_lng = $this->input->post('lng');
      $cari = $this->Am->hitung($_lat, $_lng);
      echo(json_encode($cari)); 
    }

    public function getTennant()
    {
      
      $data = $this->Am->getTennat();
      echo(json_encode($data)); 
    }

    public function setRide()
    {
      $data = array(
        'id_pengguna' => $this->input->post('id_pengguna'),
        'lat' => $this->input->post('lat'),
        'lng' => $this->input->post('lng'),
        'token' => $this->input->post('token'),
        'waktu_mulai' => date('Y-m-d H:i:s'),
      );
      $cari = $this->Am->setRide($data);
      echo(json_encode($cari)); 
    }

    
    public function endRide()
    {
      $data = array(
        'id_log' => $this->input->post('id_log'),
        'kecepatan' => $this->input->post('kecepatan'),
        'lama_berkendara' => $this->input->post('lama_berkendara'),
        'lat_akhir' => $this->input->post('lat_akhir'),
        'lng_akhir' => $this->input->post('lng_akhir'),
        'waktu_akhir' => date('Y-m-d H:i:s'),
      );
      $cari = $this->Am->endRide($data);
      echo(json_encode($cari)); 
    }
    
    public function area_notif()
    {
      header("allow-control-access-origin: * ");
      $data = json_decode(file_get_contents("php://input"), true);
      
      $_lat = $data['lat'];
      $_lng = $data['lng'];
      $_idpengguna = $data['id_pengguna'];

      $cari = $this->Am->area_notif($_lat, $_lng, $_idpengguna);
      
      echo(json_encode($cari)); 
    }
    
    public function police_notif()
    {
      header("allow-control-access-origin: * ");
      $data = json_decode(file_get_contents("php://input"), true);
      
      $_lat = $data['lat'];
      $_lng = $data['lng'];

      $cari = $this->Am->police_notif($_lat, $_lng);
      
      echo(json_encode($cari)); 
    }
    
    public function allow_notif_police()
    {
      $this->load->model('User_model', 'um');
      
      header("allow-control-access-origin: * ");
      $data = json_decode(file_get_contents("php://input"), true);

      $_idpengguna = $this->input->post('id_pengguna');

      $data_in = array(
        'token' =>  $this->input->post('token'),
       );
      $cari = $this->um->update_token($data_in, $_idpengguna);
      
      echo(json_encode($cari)); 
    }

}

/* End of file Android.php */
