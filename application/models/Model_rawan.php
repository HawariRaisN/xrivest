<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_rawan extends CI_Model {

    function getRawan()
    {
        $query = $this->db->get('rawan');
        return $query->result_array();
    }

    function proses_input_data($data){
        $res = $this->db->insert("rawan",$data);
        return $res;
    }

    function get_data_where($where){        
        $data = $this->db->get_where("rawan",$where);
        return $data->result_array();
    }

    function proses_edit_data($id_rawan,$data){
        $this->db->where(array('id_rawan' => $id_rawan));
        $res = $this->db->update('rawan',$data);
        return $res;
    }
    
    function proses_delete_data($where){
        $this->db->where($where);
        $this->db->delete("rawan");
        
    }

    function getPoly()
    {
        $query = $this->db->query("SELECT * FROM rawan WHERE type='kecelakaan' AND grup='11'");
        return $query->result_array();
    }

    function getPolydua()
    {
        $query = $this->db->query("SELECT * FROM rawan WHERE type='kecelakaan' AND grup='22'");
        return $query->result_array();
    }

    function getZonasi()
    {
        $query = $this->db->query("SELECT grup FROM rawan GROUP BY (grup)");
        $data = $query->result_array();
        $hitung = count($data);
        
        //die(print_r(count($data))); 
        
        //die(print_r($data3));
        $data4= array();
        for ($i=0; $i < $hitung; $i++) { 
            $query2 = "SELECT grup,nama_lokasi FROM rawan WHERE type='kecelakaan' AND grup=? ";
            $data2 = $this->db->query($query2, array($data[$i]['grup']));
            $data3 = $data2->result_array();

            array_push($data4,$data3);

            $data3 = '';

            
        }
        die(print_r($data4));
      

    }

    function getZonasiKecelakaan()
    {
        $query = $this->db->query("SELECT grup FROM rawan where type='kecelakaan' GROUP BY (grup)");
        $data = $query->result_array();
        $hitung = count($data);
        
        //die(print_r(($data))); 
        
        //die(print_r($data3));
        $data4= array();
        for ($i=0; $i < $hitung; $i++) { 
            $query2 = "SELECT grup,nama_lokasi,lat,lng,type FROM rawan WHERE type='kecelakaan' AND grup=? ";
            $data2 = $this->db->query($query2, array($data[$i]['grup']));
            $data3 = $data2->result_array();

            array_push($data4,$data3);

            $data3 = '';

            
        }
        //die(print_r(($data4)));
        return $data4;

    }

}

/* End of file Model_pos.php */
