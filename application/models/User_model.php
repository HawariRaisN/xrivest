<?php  
  defined('BASEPATH') OR exit('No direct script access allowed');  
  
  class User_model extends CI_Model  
  {  
   public function insertuser($data)  
   {  
    return $this->db->insert('root', $data);  
   }  

   public function changeActiveState($key)
    {
      $data = array(
        'status' => "1"
      );

      $data_key = explode("-1xx", $key);

      $this->db->from('root');

      $this->db->where('id',$data_key[1]);
      $this->db->update('root', $data);

      return TRUE;
    }

    public function add_account($data)
    {
      
      $tanggal_sekarang = date('Y-m-d');
      $tanggal_expaired = mktime(0,0,0,date("n")+3,date("j"),date("Y"));
      $tanggal_expaired = date('Y-m-d', $tanggal_expaired);

      // $diff   = $end - $start;
      // echo 'Tersisa ' . floor($diff / (60 * 60 * 24 * 30)) . ' bulan ';
      // echo floor($diff / (60 * 60 * 24)) . ' hari';

      $this->db->insert('root',$data);

      $insert_id = $this->db->insert_id();

      $data_ = array(
        'id_tenant' => $insert_id, 
        'aktif' => $tanggal_sekarang,
        'expaired' => $tanggal_expaired
      );

      $this->db->insert('des_tenant',$data_);
      
      return $insert_id;
    }

    function getPos()
    {
        $this->db->from('iklan');
        $this->db->join('root', 'iklan.id_tennant = root.id', 'left');
        $this->db->where('iklan.id_tennant', $this->session->userdata('id'));
        $query = $this->db->get(); 
        return $query->result_array();
    }

    function proses_input_data($data){
        $ten = $this->db->insert("iklan",$data);
        return $ten;
    }

    function get_data_where($where){        
        $data = $this->db->get_where("iklan",$where);
        return $data->result_array();
    }

    function proses_edit_data($id_iklan,$data){
        $this->db->where(array('id_iklan' => $id_iklan));
        $res = $this->db->update('iklan',$data);
        return $res;
    }
    
    function proses_delete_data($where){
        $this->db->where($where);
        $result = $this->db->get('iklan')->row();

        $expload = explode(base_url(), $result->gambar);
        unlink($expload[1]);

        $this->db->where($where);
        $res = $this->db->delete("iklan");
        return $res;
    }

    public function get_gambar($id_iklan)
    {
      $this->db->where('id_iklan', $id_iklan);
      return $this->db->get('iklan')->row_array();
    }
    
    
    public function update_token($data, $id)
    {
      $this->db->where('id', $id);
      $this->db->update('root',$data);
      $response['error'] = FALSE;
      $response['data'] =  "Berhasil Set Token";
      return $response;
      // return $this->db->get('iklan')->row_array();
    }
    
  }