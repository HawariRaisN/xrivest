<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Su_model extends CI_Model {

  private $table = "root";

  public function get_all()
  {
    $this->db->select('nama, username, role, id');
    
    return $this->db->get($this->table)->result();
  }

  public function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  function proses_delete_data($where){
    $this->db->where($where);
    $this->db->delete("root");
    
}

}

/* End of file ModelName.php */
