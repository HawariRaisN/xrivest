<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_android extends CI_Model {

  public function getRawan()
  {
    $data = $this->db->query("SELECT lat,lng,type,nama_lokasi FROM rawan");
    $kirim = [];
    $hasil = $data->result_array();
    for ($i=0; $i < count($hasil) ; $i++) { 
      $kirim[$i] = array(
        'lat' => $hasil[$i]['lat'] ,
        'lng' => $hasil[$i]['lng'] ,
        'type' => $hasil[$i]['type'] ,
        'nama_lokasi' => $hasil[$i]['nama_lokasi'] ,
      );
    }
   
    $response=array();
    if ($data->result() > 0) {
        $response['error'] = FALSE;
        $response['data'] = $kirim;
    } else {
        $response['error'] = TRUE;
        $response['data'] ="Data kosong";
    }
    return $response;
  }

  public function getLokasi($_lat, $_lng)
  {
    $response=array();
    if (($_lat == '') || ($_lng == '')) {
      $response['error'] = TRUE;
      $response['data'] ="Inputan Bermasalah";
    } else {
      $latFrom =  deg2rad($_lat);
      $lonFrom =  deg2rad($_lng);
     
      $earthRadius = 6371000;
      // $latFrom = deg2rad(-6.8868635);
      // $lonFrom = deg2rad(107.6153092);
      $latTo = deg2rad(-6.891878);
      $lonTo = deg2rad(107.627663);
      $lonDelta = $lonTo - $lonFrom;
      $a = pow(cos($latTo) * sin($lonDelta), 2) +
           pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
      $angle = atan2(sqrt($a), $b);
      $hitung = ceil($angle * $earthRadius);
      $response['error'] = FALSE;
      $response['data'] =  $hitung;
    }
    return $response;
  }

  public function hitung($_lat, $_lng)
  {
    $response=array();

    if (($_lat == '') || ($_lng == '')) {
      $response['error'] = TRUE;
      $response['data'] ="Inputan Bermasalah";
    } else {
        
      $lat = $_lat;
      $lng = $_lng;

      $lat1 = substr($_lat,0,4);
      $lng1 = explode(".",$_lng)[0];
      
      $data = $this->db->query("SELECT nama_lokasi, type, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM `rawan` where lat LIKE '".$lat1."%' and lng LIKE '".$lng1."%' HAVING distance < '20' ORDER BY distance ASC LIMIT 1");
      
      $hasil = $data->result_array();
      $kirim = array(
        'nama_lokasi' => $hasil[0]['nama_lokasi'] ,
        'type' => $hasil[0]['type'] ,
        'distance' => ceil($hasil[0]['distance']) ,
      );

      // $data =  $this->db->get('lokasi')->result();
      
      $response['error'] = FALSE;
      $response['data'] =  $kirim;
      //echo ;
    }
    return $response;
  }
  
  public function getTennat(){
      
      $data = $this->db->get('iklan');
      $hasil = $data->result_array();
      $kirim = [];
      
        for ($i=0; $i < count($hasil) ; $i++) { 
          
          $no_hp = $this->db->get_where('root', array('id' => $hasil[$i]['id_tennant']))->row_array()['no_hp'];
          
          $kirim[$i] = array(
            'judul' => $hasil[$i]['judul'] ,
            'keterangan' =>$hasil[$i]['keterangan'],
            'gambar' => $hasil[$i]['gambar'],
            'alamat' => $hasil[$i]['alamat'],
            'no_wa' => $no_hp
            
          );
        } 
      
      
      if($data->num_rows() > 0){
         $response['error'] = FALSE;
         $response['data'] = $kirim;
      }else{
         $response['error'] = TRUE;
         $response['data'] = "Data kosong";
      }
      
      return $response;
  }

  public function setRide($data)
  {
    $response=array();
    if ($this->db->insert('log', $data)) {
       $last = $this->db->insert_id();
      $kirim = array(
        'id_log' => $last ,
      );
      
      $response['error'] = FALSE;
      $response['data'] = $kirim;
      
    } else {
      $response['error'] = TRUE;
      $response['data'] ="Inputan Bermasalah";
    }
    return $response;
  }

  public function endRide($data)
  {
    $response=array();
    if ($this->db->insert('detail_log', $data)) {
      $response['error'] = FALSE;
      $response['data'] = "Log Tersimpan";
    } else {
      $response['error'] = TRUE;
      $response['data'] ="Inputan Bermasalah";
    }
    return $response;
  }

  public function area_notif($_lat, $_lng, $_idpengguna)
  {
    $response=array();

    if (($_lat == '') || ($_lng == '')) {
      $response['error'] = TRUE;
      $response['data'] ="Inputan Bermasalah";
    } else {
      $kirim = [];
      $lat = $_lat;
      $lng = $_lng;
      $id_pengguna = $_idpengguna;
      // $data = $this->db->query("SELECT DISTINCT(id_pengguna) as id_pengguna, ( 3959 * acos( cos( radians(-6.8868635) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(107.6131205) ) + sin( radians(-6.8868635) ) * sin( radians( lat ) ) ) ) AS distance FROM `log` WHERE DATE(waktu_mulai) = CURDATE()
      // HAVING distance < '5' ORDER BY distance ASC");

      $data = $this->db->query("SELECT DISTINCT(id_pengguna), token, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM `log` WHERE DATE(waktu_mulai) = CURDATE() AND id_pengguna !=  '".$id_pengguna."' ORDER BY distance ASC");
      
      $hasil = $data->result_array();

      if (count($hasil) == 0) {
        $response['error'] = TRUE;
        $response['data'] ="Belum Ada Pengendara disekitar";
      } else {
        for ($i=0; $i < count($hasil) ; $i++) { 
          $kirim[$i] = array(
            'id_pengguna' => $hasil[0]['id_pengguna'] ,
            'distance' =>$hasil[0]['distance'],
            'token' =>$hasil[0]['token'],
          );
        }
        
  
        // $data =  $this->db->get('lokasi')->result();
        
        $response['error'] = FALSE;
        $response['data'] =  $kirim;
        //echo ;
      }
     
    }
    return $response;
  }
  
   public function police_notif($_lat, $_lng)
  {
    $response=array();

    if (($_lat == '') || ($_lng == '')) {
      $response['error'] = TRUE;
      $response['data'] ="Inputan Bermasalah";
    } else {
      $kirim = [];
      $lat = $_lat;
      $lng = $_lng;

      $data = $this->db->query("SELECT id_pos, nama_lokasi, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM `polisi` ORDER BY distance ASC limit 1");
      
      $hasil = $data->result_array();

      if (count($hasil) == 0) {
        $response['error'] = TRUE;
        $response['data'] ="Belum Ada Polsek disekitar";
      } else {
        $this->db->where('id_pos', $hasil[0]['id_pos']);
        $hasil_akhir = $this->db->get('root')->result_array();
        
        for ($i=0; $i < count($hasil_akhir) ; $i++) { 
          $kirim[$i] = array(
            'token' => $hasil_akhir[$i]['token'] ,
          );
        }
        
  
        // $data =  $this->db->get('lokasi')->result();
        
        $response['error'] = FALSE;
        $response['data'] =  $kirim;
        //echo ;
      }
     
    }
    return $response;
  }
  

}

/* End of file Model_android.php */
