<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	private $table = "root";
	private $_data = array();

	public function validate()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$where = array(
			'username' => $username,
// 			'status' => '1' 
		);

		$this->db->where($where);
		
		$query = $this->db->get($this->table);

		if ($query->num_rows()) 
		{
			// found row by username	
			$row = $query->row_array();

			$get_tenant = $this->db->get_where('des_tenant', array('id_tenant' => $row['id']));

			if($get_tenant->num_rows() > 0){

			
				$query = $get_tenant->row();

				$data_set = array(
					'id_des' => $query->id_des,
					'expaired' => $query->expaired,
					'status_tenant' => $query->status_tenant
				);

				$this->session->set_userdata($data_set);
				
			}

			// now check for the password
			if ($row['password'] == sha1($password)) 
			{
				// we not need password to store in session
				unset($row['password']);
				$this->_data = $row;
				return "ERR_NONE";
			}

			// password not match
			return "ERR_INVALID_PASSWORD";
		}
		else {
			// not found
			return "ERR_INVALID_USERNAME";
		}
	}

	public function get_data()
	{
		return $this->_data;
	}

}