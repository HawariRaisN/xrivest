<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pos extends CI_Model {

    function getPos()
    {
        $query = $this->db->get('polisi');
        return $query->result_array();
    }

    function proses_input_data($data){
        $res = $this->db->insert("polisi",$data);
        return $res;
    }

    function get_data_where($where){        
        $data = $this->db->get_where("polisi",$where);
        return $data->result_array();
    }

    function proses_edit_data($id_pos,$data){
        $this->db->where(array('id_pos' => $id_pos));
        $res = $this->db->update('polisi',$data);
        return $res;
    }
    
    function proses_delete_data($where){
        $this->db->where($where);
        $res = $this->db->delete("polisi");
        return $res;
    }

}

/* End of file Model_pos.php */
