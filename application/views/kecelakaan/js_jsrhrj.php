
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/datatables/datatables.min.js"></script>

   
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase.js"></script>
<script>

    
// $('.exit').click(function(){
//     document.getElementById('myModal').setAttribute.('aria-hidden', 'true');
//     $('#myModal').removeClass('show');
//     //alert('ok');
// });

// Initialize Firebase
var config = {
    apiKey: "AIzaSyB4Ddoz4G4P9KnjLrSI8XF5R0vqm3M4qB0",
    authDomain: "rivest-4507.firebaseapp.com",
    databaseURL: "https://rivest-4507.firebaseio.com",
    projectId: "rivest-4507",
    storageBucket: "rivest-4507.appspot.com",
    messagingSenderId: "1062631027"
};
firebase.initializeApp(config);

var db = firebase.database();
var kecelakaanRef = db.ref('accidents');
var penggunaRef = db.ref('pengguna');
// var kecelakaanRefWhere = db.ref('alat')
// ---show data----
kecelakaanRef.on('value', showData, showError);
function showData(data) {
    var _table = document.getElementById('myTable');
    var _tambah = '';
    var no = 1;
    var st ='-';

    data.forEach(function(child){
      if ((child.val().status == "Sudah Ditangani")&&(child.val().keterangan != "-")) {
        if (child.val().status_kecelakaan == "-") {
            st = "<td> - </td>"
        } else {
            st = "<td><a href='#' onclick='detailSurat(\"" +child.key+ "\")' data-toggle='modal' data-target='#myModal3'><i class='fa fa-book' title='Detail'></i></a></td>"
        }
        _tambah += "<tr>" 
                      +"<td>" + no + "</td>" 
                      +"<td>" + child.val().nama + "</td>" 
                      +"<td>" + child.val().keterangan + "</td>" 
                      + st
                      +"<td>" + child.val().status_kecelakaan + "</td>"
                

                      +"<td><a href='#' onclick='detailKecelakaan(\"" +child.key+ "\")' data-toggle='modal' data-target='#myModal'><i class='fa fa-search-plus' title='Detail'></i></a> </td>"
                  +"</tr>";
        no++;
      } 

      
    })
    _table.innerHTML=_tambah;
    $('#example').DataTable();  
}

function showError(err) {
    console.log(err.val())
}
// ---show data----


function detailKecelakaan(id) {
        kecelakaanRef.child(id).once('value', function(datanya){
          penggunaRef.child(datanya.val().id_pengguna).once('value', function(snap){
            
            $("#contact").html(snap.val().kontak_pengguna)
            $("#contact2").html(snap.val().kerabat_pengguna)
            $("#dvp").html(snap.val().kendaraan_pengguna)
        })
            $("#nameee").html(datanya.val().nama)
            $("#rin").html(datanya.val().id_alat)
            $("#location").html(datanya.val().lokasi)
            $("#type").html(datanya.val().tipe)
            $("#date").html(datanya.val().tanggal)
            //$("#keterangan").html(datanya.val().tanggal)
            //$("#keterangan").prop('disabled', true)
        })
    }

    function detailSurat(id) {
        kecelakaanRef.child(id).once('value', function(datanya){
          penggunaRef.child(datanya.val().id_pengguna).once('value', function(snap){
            
            $("#nama").html(snap.val().nama_pengguna)
            $("#alamat").html(snap.val().alamat_pengguna)
            $("#jk").html(snap.val().kelamin_pengguna)
            $("#jk").html(snap.val().tempat_pengguna)
        })
            $("#tanggal").html(datanya.val().tanggal)
            $("#lokasi").html(datanya.val().lokasi)
            $("#keterangan").html(datanya.val().keterangan)
            //$("#keterangan").html(datanya.val().tanggal)
            //$("#keterangan").prop('disabled', true)
        })
    }

     function editChronology(id) {
        kecelakaanRef.child(id).once('value', function(datanya){
            if (datanya.val().status_kecelakaan == "-") {
                $("#id_m").val(id)
            } else {
                $("#kecelakaan1").prop('disabled', true)
                $("#kecelakaan2").prop('disabled', true)
            }
            
        })
    }

     function actionEditKecelakaanTunggal() {
        var id = $("#id_m").val()
        kecelakaanRef.child(id).update({
            status_kecelakaan : "Kecelakaan Tunggal"
        })

         $(function () {
        var data = '<?= "Edit Data Success" ?>';
         toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "rtl": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": 300,
          "hideDuration": 1000,
          "timeOut": 5000,
          "extendedTimeOut": 1000,
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        toastr.success(data);
      });
    }

     function actionEditKecelakaanDua() {
        var id = $("#id_m").val()
        kecelakaanRef.child(id).update({
            status_kecelakaan : "Kecelakaan Dua Kendaraan"
        })

         $(function () {
        var data = '<?= "Edit Data Success" ?>';
         toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "rtl": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": 300,
          "hideDuration": 1000,
          "timeOut": 5000,
          "extendedTimeOut": 1000,
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        toastr.success(data);
      });
    }

// // ---Tambah Data---

// $("#tambah_alat").click(function(){
//     var ssid = $("#ssid");
//     var sts = $("#status");
//     if ((ssid.val() =='') || (sts.val()=='')) {
//         $(function () {
//             var data = '<?= "Failed to Add Data Success" ?>';
//             toastr.options = {
//             "closeButton": false,
//             "debug": false,
//             "newestOnTop": false,
//             "progressBar": false,
//             "rtl": false,
//             "positionClass": "toast-top-right",
//             "preventDuplicates": false,
//             "onclick": null,
//             "showDuration": 300,
//             "hideDuration": 1000,
//             "timeOut": 5000,
//             "extendedTimeOut": 1000,
//             "showEasing": "swing",
//             "hideEasing": "linear",
//             "showMethod": "fadeIn",
//             "hideMethod": "fadeOut"
//             }
//             toastr.error(data);
//         });
//     } else {
        
//         kecelakaanRef.push({
//         ssid : ssid.val(),
//         status : sts.val()
//         });

//         ssid.val(''); 
//         sts.val('');

//         $(function () {
//         var data = '<?= "Add Data Success" ?>';
//             toastr.options = {
//             "closeButton": false,
//             "debug": false,
//             "newestOnTop": false,
//             "progressBar": false,
//             "rtl": false,
//             "positionClass": "toast-top-right",
//             "preventDuplicates": false,
//             "onclick": null,
//             "showDuration": 300,
//             "hideDuration": 1000,
//             "timeOut": 5000,
//             "extendedTimeOut": 1000,
//             "showEasing": "swing",
//             "hideEasing": "linear",
//             "showMethod": "fadeIn",
//             "hideMethod": "fadeOut"
//             }
//             toastr.success(data);
//         });

//     }

// });


// function editAlat(id) {
//     kecelakaanRef.child(id).once('value', function(datanya){
//         $("#ssid_m").val(datanya.val().ssid)
//         $("#status_m").val(datanya.val().status)
//         $("#id_m").val(id)
//     })
// }

// function actionEdit() {
//     var id = $("#id_m").val()
//     kecelakaanRef.child(id).update({
//         ssid : $("#ssid_m").val(),
//         status : $("#status_m").val()
//     })
    
//     $("#ssid_m").val('')
//     status : $("#status_m").val('')

//      $(function () {
//     var data = '<?= "Edit Data Success" ?>';
//      toastr.options = {
//       "closeButton": false,
//       "debug": false,
//       "newestOnTop": false,
//       "progressBar": false,
//       "rtl": false,
//       "positionClass": "toast-top-right",
//       "preventDuplicates": false,
//       "onclick": null,
//       "showDuration": 300,
//       "hideDuration": 1000,
//       "timeOut": 5000,
//       "extendedTimeOut": 1000,
//       "showEasing": "swing",
//       "hideEasing": "linear",
//       "showMethod": "fadeIn",
//       "hideMethod": "fadeOut"
//     }
//     toastr.success(data);
//   });
// }

// function delAlat(id) {
//     kecelakaanRef.child(id).once('value', function(datanya){
//         $("#id_d").val(id)
//         // console.log(id)
//     })
// }

//  function actionDel() {
//     var id = $("#id_d").val();
//     console.log(id)
//     kecelakaanRef.child(id).remove()

//      $(function () {
//     var data = '<?= "Delete Data Success" ?>';
//      toastr.options = {
//       "closeButton": false,
//       "debug": false,
//       "newestOnTop": false,
//       "progressBar": false,
//       "rtl": false,
//       "positionClass": "toast-top-right",
//       "preventDuplicates": false,
//       "onclick": null,
//       "showDuration": 300,
//       "hideDuration": 1000,
//       "timeOut": 5000,
//       "extendedTimeOut": 1000,
//       "showEasing": "swing",
//       "hideEasing": "linear",
//       "showMethod": "fadeIn",
//       "hideMethod": "fadeOut"
//     }
//     toastr.success(data);
//   });
// }

</script>

</body>
</html>