<!-- Page wrapper  -->
<div class="page-wrapper">
  <!-- Bread crumb -->

  <div class="row page-titles">
    <div class="col-md-5 align-self-center">
      <h3 class="text-primary"><?= $s1 ?></h3>
    </div>
    <div class="col-md-7 align-self-center">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
        <li class="breadcrumb-item active"><?= $s1 ?></li>
      </ol>
    </div>
  </div>
  <!-- End Bread crumb -->


  <!-- Container fluid  -->
  <div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div id="map"></div>
        </div>
      </div>
     
    </div>
    <!-- End PAge Content -->

   
    <!-- End Container fluid  -->

  </div>
  <!-- End Page wrapper  -->
  <!-- footer -->

  <!-- End footer -->