
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">List of <?= $s1 ?></h6>
                        <div class="table-responsive m-t-40">
                        <table class="table table-bordered table-striped table-condensed flip-content" id="example">
                            <thead class="flip-content">
                              <tr>
                                  <td>No</td>
                                  <td>Name</td>
                                  <td>Chronology</td>
                                  <td>Letter Of Accident</td>
                                  <td>Accident Status</td>
                                  <td>Detail</td>
                              </tr>
                            </thead>
                            <tbody id="myTable">
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>No</td>
                                  <td>Name</td>
                                  <td>Chronology</td>
                                  <td>Letter Of Accident</td>
                                  <td>Accident Status</td>
                                  <td>Detail</td>
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
        <!-- Modal -->  
        <div class="row">
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Form <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                    <table class="table">
                        <tr><td>Type</td><td><span id="type"></span></td></tr>
                        <tr><td>Name</td><td><span id="nameee"></span></td></tr>
                        <tr><td>Contact</td><td><span id="contact"></span></td></tr>
                        <tr><td>Family Contact</td><td><span id="contact2"></span></td></tr>
                        <tr><td>Driver's Vehicle  plate</td><td><span id="dvp"></span></td></tr>
                        <tr><td>Rivest ID Number</td><td><span id="rin"></span></td></tr>
                        <tr><td>Location</td><td><span id="location"></span></td></tr>
                        <tr><td>Date</td><td><span id="date"></span></td></tr>
                        <!-- <form role="form" id="form-validation1" action="#" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="ssid_m" id="ssid_m" class="form-control input-md" placeholder="SSID Name" tabindex="1" data-error="SSID must be entered" required></div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="status_m" id="status_m" class="form-control input-md" placeholder="Sudah/Belum Aktiv" tabindex="2" data-error="Status must be entered" required>
                                        <input type="hidden" name="id_m" id="id_m">
                                    </div>
                                </div>
                            </div>                                
                            
                        </form> -->
                        <!-- <tr>
                            <td>Chronology</td>
                            <td> 
                                <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                            </td>
                        </tr> -->
                    </table>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        
                    </div>

                    </div>
                </div>
            </div>
            <div class="modal" id="myModal2">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Make a Decission</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <table>
                                        <tr>
                                            <button class="btn btn-success" data-dismiss="modal" onclick="actionEditKecelakaanTunggal()" id="kecelakaan1">Kecelakaan Tunggal</button>
                                            <input type="hidden" name="id_m" id="id_m">
                                        </tr>
                                        
                                    </table>   
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <table>
                                        <tr>
                                            <button class="btn btn-success" data-dismiss="modal" onclick="actionEditKecelakaanDua()" id="kecelakaan2">Kecelakaan Dua Kendaraan</button>
                                            <input type="hidden" name="id_m" id="id_m">
                                        </tr>
                                        
                                    </table>   
                                </div>
                            </div>
                        </div>                              
                    </div>
                    

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        
                    </div>

                    </div>
                </div>
            </div>
            <div class="modal" id="myModal3">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Surat Keterangan Kecelakaan Lalu Lintas</h4>
                        
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                  Yang bertanda tangan di bawah ini : <br>
                                  <table>
                                    <tr>
                                        <td> Nama</td>
                                        <td>  :</td>
                                        <td>Kikit Teguh Waluyo</td>
                                    </tr>
                                    <tr>
                                        <td> Jabatan</td>
                                        <td>  :</td>
                                        <td>Korlap</td>
                                    </tr>
                                  </table>
                                  Menerangkan dengan sebenarnya bahwa telah terjadi kecelakaan yang menimpan seseorang atas nama : <br>
                                  <table>
                                    <tr>
                                        <td> Nama</td>
                                        <td>  :</td>
                                        <td><span id="nama"></span></td>
                                    </tr>
                                    <tr>
                                        <td> Alamat</td>
                                        <td>  :</td>
                                        <td><span id="alamat"></span></td>
                                    </tr>
                                    <tr>
                                        <td> Jenis Kelamin </td>
                                        <td>  :</td>
                                        <td><span id="jk"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Tempat</td>
                                        <td>  :</td>
                                        <td><span id="tempat_lahir"></span></td>
                                    </tr>
                                  </table>
                                  Adapun uraian kejadiannya adalah sebagai berikut :
                                  <table>
                                    <tr>
                                        <td>1. Kecelakaan terjadi pada <span id="tanggal"></span> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>2. Lokasi kejadiannya <span id="lokasi"></span></td>
                                        <td> </td>
                                    </tr>
                                    <tr>
                                        <td>3. Kronologi <span id="keterangan"></span></td>
                                        <td></td>
                                    </tr>
                                   
                                  </table>
                                  Demikian Surat ini dibuat dengan sebenarnya untuk dipergunakan sebagaimana mestinya.
                                </div>
                            </div>
                        
                        </div>                              
                    </div>
                    

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Container fluid  -->
    
</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        