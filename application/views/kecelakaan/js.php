        <?php 
            $alamat_service = base_url('assets/js/service-worker.js');
        ?>
        <script src="<?php echo base_url('src/backend/'); ?>js/lib/datatables/datatables.min.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase-database.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase-messaging.js"></script>
        
       
        <script>
        // Initialize Firebase
        
        var config = {
            apiKey: "AIzaSyB4Ddoz4G4P9KnjLrSI8XF5R0vqm3M4qB0",
            authDomain: "rivest-4507.firebaseapp.com",
            databaseURL: "https://rivest-4507.firebaseio.com",
            projectId: "rivest-4507",
            storageBucket: "rivest-4507.appspot.com",
            messagingSenderId: "1062631027"
        };

        
        firebase.initializeApp(config);
        
        
        
        var db = firebase.database();
        var accidents = db.ref().child('accidents');

        const messaging = firebase.messaging();
        
        
        window.addEventListener('load', () => {
            
            if ('serviceWorker' in navigator) {
                
                refrestoToken();
        
                navigator.serviceWorker.register('<?= base_url("firebase-messaging-sw.js") ?>')
                    .then(registration => {
        
                        messaging.useServiceWorker(registration)
                        requestPermission();
                        
                    })
                    .catch(err => console.log('Service Worker Error', err))
        
            } else {
                console.log('Browser not support');
            }
        
        })
        

        var map, markers = [], bounds;
        var button = document.getElementById('kirim');
        var lat = document.getElementById('lat');
        var lng = document.getElementById('lng');
        var citymap = {
            chicago: {
            population: 100
        },
        };

        // button.addEventListener('click', kirimData);

        // function kirimData(e) {
        //     e.preventDefault();

        //     accidents.push({
        //         id_pengguna:2,
        //         lat:lat.value,
        //         lng:lng.value,
        //         status:'Belum Ditangani',
        //         tanggal:'2018-03-27'
        //     });
        // }
        
        function refrestoToken(){
            messaging.onTokenRefresh(function() {
              messaging.getToken().then(function(refreshedToken) {
                console.log('Token refreshed.');
                insertToken(refreshedToken);
              }).catch(function(err) {
                console.log('Unable to retrieve refreshed token ', err);
              });
            });
        }
        
        function requestPermission(){
            console.log('Masuk permisson');
            messaging.requestPermission()
                .then(() => messaging.getToken())
                .then(token => {
                    insertToken(token);
            })
            .catch(err => console.log('Denied', err))
        }
        
        function insertToken(token){
            
            console.log(token);
            
            var id_pengguna = <?= $this->session->userdata('id') ?>;
            var url = '<?= site_url("and/settoken") ?>';
            
            $.ajax({
                url:url,
                method:'POST',
                data:{id_pengguna:id_pengguna,token:token},
                typeData:'json',
                success:function(data){
                    console.log(data);
                },
                error:function(err){
                    console.log(err);
                }
            });
                
            
        }


        function getAwal(){
        
            accidents.once('value', function(snap){

                // console.log(markers);
                
                
                data = snap.val();

                
                    for(k in data){

                        file = data[k];

                        if(file.status == "Belum Ditangani"){
                            var img23 = "<?= base_url('src/marker/kecelakaan.png') ?>";
                            var latlng = new google.maps.LatLng(parseFloat(file.lat), parseFloat(file.lng));
                            
                            var infowindowContent = 'Waktu kejadian :' + file.tanggal;

                            bounds.extend(latlng);
                            
                            console.log(img23);

                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                                icon: img23
                            });
                            
                            openInfo(marker, infowindowContent);

                            markers.push(marker);

                            map.fitBounds(bounds);
                            
                        }

                        

                    }

                    

                
                    
            });
        }



        function initMap() {
            
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -6.91746390, lng: 107.61912280},
                zoom: 13,
                mapTypeControl: false
            });

        
            bounds = new google.maps.LatLngBounds();
            var infoWindow = new google.maps.InfoWindow;

            <?php foreach($pos as $data){
            
            $nama = $data['nama_lokasi'];
            $lat = $data['lat'];
            $lon = $data['lng'];
            $ket = $data['keterangan'];
            $image= base_url('src/marker/pospolisi.png');
    
            echo ("posMarker($lat, $lon,'$image', 'Nama : $nama<br/>Keterangan : $ket');\n");                                  
        } ?>

        function posMarker(lat, lng, img, info) {
            var lokasi = new google.maps.LatLng(lat, lng);
            bounds.extend(lokasi);
            var marker = new google.maps.Marker({
                map: map,
                position: lokasi,
                icon: img
            });       
            map.fitBounds(bounds);
            bindInfoWindow(marker, map, infoWindow, info);
        }

        <?php foreach($rawan as $data){?>
            for (var city in citymap) {
                var cityCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                center: {lat: <?=$data['lat']?>, lng: <?=$data['lng']?>},
                radius: Math.sqrt(citymap[city].population) * 20
                });
            }
            <?php } ?>

            <?php foreach($rawan as $data){
            
            $nama = $data['nama_lokasi'];
            $lat = $data['lat'];
            $lon = $data['lng'];
            $ket = $data['keterangan'];
            $type = $data['type'];

            if ($data['type']=="Kecelakaan") {
                $image= base_url('src/marker/rawanlaka.png');
                //$image= base_url('src/marker/kecelakaan.png');
            } else if ($data['type']=="Pembegalan") {
                $image= base_url('src/marker/rawanlaka.png');
                //$image= base_url('src/marker/rawanlaka.png');
            } else if ($data['type']=="Pohon Tumbang") {
                $image= base_url('src/marker/rawanlaka.png');
                //$image= base_url('src/marker/rawanlaka.png');
            } else{
                $image= base_url('src/marker/rawanlaka.png');
                //$image= base_url('src/marker/rawanlaka.png');
            }
            
            echo ("rawanMarker($lat, $lon,'$image', 'Lokasi : $nama<br/>Keterangan : $ket<br/>Rawan : $type');\n");                                  
            } ?>

            function rawanMarker(lat, lng, img, info) {
                var lokasi = new google.maps.LatLng(lat, lng);
                bounds.extend(lokasi);
                var marker = new google.maps.Marker({
                    map: map,
                    position: lokasi,
                    icon: img
                });       
                map.fitBounds(bounds);
                bindInfoWindow(marker, map, infoWindow, info);
            }

        function bindInfoWindow(marker, map, infoWindow, html) {
          google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
          });
        }

            accidents.on('child_added', addMarker);

            accidents.on('child_changed', changeMarker);

            accidents.once('value', function(snap){
                
                // if(markers.length > 0){
                //     reset_map();
                // }

                console.log('Awal');
                
                
                data = snap.val();

                
                    for(k in data){

                        file = data[k];

                        if(file.status == "Belum Ditangani"){
                            var latlng = new google.maps.LatLng(parseFloat(file.lat), parseFloat(file.lng));
                            var img23 = "<?= base_url('src/marker/kecelakaan.png') ?>";
                            var infowindowContent = 'Waktu kejadian :' + file.tanggal;

                            bounds.extend(latlng);

                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                                icon:img23,
                                animation: google.maps.Animation.DROP
                            });
                            
                            openInfo(marker, infowindowContent);

                            map.fitBounds(bounds);
                            
                        }

                    }

                    
                    
            });
            
                
        }

        function changeMarker(snap){

            reset_map();
        }

        function addMarker(snap) {

                var data = snap.val();
                    
                if(data.status == "Belum Ditangani"){
                    var latlng = new google.maps.LatLng(parseFloat(data.lat), parseFloat(data.lng));
                    
                    var infowindowContent = 'Waktu kejadian :' + data.tanggal;

                    bounds.extend(latlng);

                    var marker = new google.maps.Marker({
                        position: latlng,
                            map: map,
                            animation: google.maps.Animation.DROP
                        });
                        
                    openInfo(marker, infowindowContent);

                    markers.push(marker);

                    map.fitBounds(bounds);
                }
                
            
        }

        function openInfo(mark, infowindowContent){
            
            var infoWindow = new google.maps.InfoWindow({
                content: infowindowContent, 
                maxwidth : 400
            });
            

            google.maps.event.addListener(mark, 'click', function() {
                infoWindow.open(map, mark);
            })
        }

        function reset_map() {
            

            for(i in markers){

                markers[i].setVisible(false);
            }

            markers =[];

            getAwal();
            
        }
        
        function registerServiceWorker(){
           
        }
 
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpALWzkNO7VH_pCSX30bt43_7h3sIeqQI&libraries=places&callback=initMap" async defer></script>
        
        <?php 
                if ($this->session->userdata('role') == 'Police') 
                { 
        ?>

        <script>
           
            var alatRef = db.ref('accidents');
            // var alatRefWhere = db.ref('alat')
            // ---show data----
            alatRef.on('value', showData, showError);
            alatRef.on('child_added', tambah);
            alatRef.on('child_changed', ubah)
            var x = document.getElementById("myAudio"); 
            
            function showData(data) {
                var _table = document.getElementById('myTable');
                var _tambah = '';
                var no = 1;

                data.forEach(function(child){
                    if (child.val().status == "Belum Ditangani") {
                        var statu = "<td style='background-color:red; color:white'>" + child.val().status + "</td>"
                        var tangani = "<td><a href='#' onclick='editKec(\"" +child.key+ "\")'  data-toggle='modal' data-target='#myModal2'><i class='fa fa-pencil-square-o'></i></a></td>"
                    } else {
                        var statu = "<td style='background-color:green;color:white'>" + child.val().status + "</td>"
                        var tangani = "<td>-</td>"
                    }

                   _tambah += "<tr>" 
                                    +"<td>" + no + "</td>" 
                                    +"<td>" + child.val().nama + "</td>" 
                                    + statu 
                                    +"<td>" + child.val().tanggal + "</td>" 
                                    +"<td>" + child.val().lokasi + "</td>" 
                                    +tangani
                                    +"<td style='text-align:center'><a href='/k/dijkstra'> Rute </td>"
                                +"</tr>";
                    no++;
                })
                _table.innerHTML=_tambah;
                $('#example').DataTable();  
            }

            function showError(err) {
                console.log(err.val())
            }
            // ---show data----

            function editKec(id) {
                alatRef.child(id).once('value', function(datanya){
                    $("#id_d").val(id)
                    // console.log(id)
                })
            }

            function actionKec() {
                var id = $("#id_d").val()
                alatRef.child(id).update({
                    status : "Sudah Ditangani"
                })
                x.pause(); 
            }


            function tambah(data) {
                if (data.val().status == "Belum Ditangani") {
                    x.play();

                    // alert('Terjadi kecelakaan pada '+data.val().lokasi+' Waktu : '+data.val().waktu);
                  
                }  
            }

             function ubah(data) {
                if (data.val().status == "Belum Ditangani") {
                    x.play();

                    // alert('Terjadi kecelakaan pada '+data.val().lokasi+' Waktu : '+data.val().waktu);
                  
                }  
            }

            
            
            


            // function editKec(id) {
            //     alatRef.child(id).once('value', function(datanya){
            //         $("#id_d").val(id)
            //         // console.log(id)
            //     })
            // }

           
        </script>
        <?php } 
                    else { ?>

                               <script>
           
                                var alatRef = db.ref('accidents');
                                // var alatRefWhere = db.ref('alat')
                                // ---show data----
                                alatRef.on('value', showData, showError);
                                alatRef.on('child_added', tambah);
                                alatRef.on('child_changed', ubah)
                                var x = document.getElementById("myAudio"); 
                                function showData(data) {
                                    var _table = document.getElementById('myTable');
                                    var _tambah = '';
                                    var no = 1;

                                    data.forEach(function(child){
                                        if (child.val().status == "Belum Ditangani") {
                                            var statu = "<td style='background-color:red; color:white'>" + child.val().status + "</td>"
                                            var tangani = "<td>-</td>"
                                        } else {
                                            var statu = "<td style='background-color:green;color:white'>" + child.val().status + "</td>"
                                            var tangani = "<td>-</td>"
                                        }

                                        _tambah += "<tr>" 
                                                        +"<td>" + no + "</td>" 
                                                        +"<td>" + child.val().nama + "</td>" 
                                                        + statu 
                                                        +"<td>" + child.val().tanggal + "</td>" 
                                                        +"<td>" + child.val().lokasi + "</td>" 
                                                        +tangani
                                                    +"</tr>";
                                        no++;
                                    })
                                    _table.innerHTML=_tambah;
                                    $('#example').DataTable();  
                                }

                                function showError(err) {
                                    console.log(err.val())
                                }
                                // ---show data----

                                function editKec(id) {
                                    alatRef.child(id).once('value', function(datanya){
                                        $("#id_d").val(id)
                                        // console.log(id)
                                    })
                                }

                                function actionKec() {
                                    var id = $("#id_d").val()
                                    alatRef.child(id).update({
                                        status : "Sudah Ditangani"
                                    })
                                    x.pause(); 
                                }


                                function tambah(data) {
                                    if (data.val().status == "Belum Ditangani") {
                                        x.play();

                                        // alert('Terjadi kecelakaan pada '+data.val().lokasi+' Waktu : '+data.val().waktu);
                                        
                                    }  
                                }

                                    function ubah(data) {
                                    if (data.val().status == "Belum Ditangani") {
                                        x.play();

                                        // alert('Terjadi kecelakaan pada '+data.val().lokasi+' Waktu : '+data.val().waktu);
                                        
                                    }  
                                }
                                
                                


                                // function editKec(id) {
                                //     alatRef.child(id).once('value', function(datanya){
                                //         $("#id_d").val(id)
                                //         // console.log(id)
                                //     })
                                // }

                                
                            </script>

                   <?php }
                
                
                ?>

</body>
</html>