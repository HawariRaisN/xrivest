
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div id="map"></div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">List of <?= $s1 ?></h6>
                        <div class="table-responsive m-t-40">
                            <table class="table table-bordered table-striped table-condensed flip-content" id="example">
                                <thead class="flip-content">
                                    <tr>
                                        <td>No</td>
                                        <td>Nama Pengguna</td>
                                        <td>Status</td>
                                        <td>Waktu</td>
                                        <td>Lokasi</td>
                                        <td>Tangani</td>
                                        <td>Detail</td>
                                    </tr>
                                </thead>
                                <tbody id="myTable">
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>No</td>
                                        <td>Nama Pengguna</td>
                                        <td>Status</td>
                                        <td>Waktu</td>
                                        <td>Lokasi</td>
                                        <td>Tangani</td>
                                        <td>Detail</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->

        <div class="row">
            <div class="modal" id="myModal2">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title"> Action <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form role="form" id="form-validation1" action="#" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <h2> Handle this ? </h2>
                                        <input type="hidden" id="id_d" name="id_d">
                                    </div>
                                </div>
                            </div>                                
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <a href="#" class="btn btn-success " data-dismiss="modal" onclick="actionKec()">Yes</a>
                    </div>

                    </div>
                </div>
            </div>
    </div>
    <!-- End Container fluid  -->
    
    

    <audio id="myAudio">
        <source src="<?php echo base_url('src/') ;?>alert.mp3" type="audio/mpeg">
        Your browser does not support the audio element.
    </audio>

</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        