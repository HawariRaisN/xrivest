
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpALWzkNO7VH_pCSX30bt43_7h3sIeqQI&libraries=places&callback=initMap" async defer></script>
        
        <script>
           
var citymap = {
    chicago: {
    population: 100
  },
};

function initMap() {

  var infoWindow = new google.maps.InfoWindow;
  var bounds = new google.maps.LatLngBounds(); 
  
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: -6.885500, lng: 107.615407},
    mapTypeId: 'roadmap'
  });

  <?php foreach($konten as $data){?>
  for (var city in citymap) {
    var cityCircle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: {lat: <?=$data['lat']?>, lng: <?=$data['lng']?>},
      radius: Math.sqrt(citymap[city].population) * 20
    });
  }
  <?php } ?>

  <?php foreach($konten as $data){
  
  $nama = $data['nama_lokasi'];
  $lat = $data['lat'];
  $lon = $data['lng'];
  $ket = $data['keterangan'];
  $type = $data['type'];

  if ($data['type']=="Kecelakaan") {
      $image= base_url('src/marker/rawanlaka.png');
  } else if ($data['type']=="Pembegalan") {
      $image= base_url('src/marker/rawanlaka.png');
  } else if ($data['type']=="Pohon Tumbang") {
      $image= base_url('src/marker/rawanlaka.png');
  } else{
      $image= base_url('src/marker/rawanlaka.png');
  }
  
  echo ("addMarker($lat, $lon,'$image', 'Lokasi : $nama<br/>Keterangan : $ket<br/>Rawan : $type');\n");                                  
} ?>

function addMarker(lat, lng, img, info) {
    var lokasi = new google.maps.LatLng(lat, lng);
    bounds.extend(lokasi);
    var marker = new google.maps.Marker({
        map: map,
        position: lokasi,
        icon: img
    });       
    map.fitBounds(bounds);
    bindInfoWindow(marker, map, infoWindow, info);
  }

  function bindInfoWindow(marker, map, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function() {
      infoWindow.setContent(html);
      infoWindow.open(map, marker);
    });
  }


}

</script>



</body>
</html>