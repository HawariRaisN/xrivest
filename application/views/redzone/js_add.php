
<!-- <script type="text/javascript">
    document.getElementById('reset').onclick= function()
    {
        var field1= document.getElementById('lng');
        var field2= document.getElementById('lat');
        field1.value= field1.defaultValue;
        field2.value= field2.defaultValue;
    };
</script>     -->
<script type="text/javascript">
    function updateMarkerPosition(latLng, address) 
    {
      document.getElementById('lat').value = [latLng.lat()];
      document.getElementById('lng').value = [latLng.lng()];
      document.getElementById('nama_lokasi').value = address;
    }

    function initMap() {

        var myOptions = 
        {
            zoom: 14,
            scaleControl: true,
            center:  new google.maps.LatLng(-6.918782, 107.603329),
            mapTypeId: 'roadmap'
        };

        var input = document.getElementById('pac-input');

        
    
        var map = new google.maps.Map(document.getElementById("map"),myOptions); 

        var marker1 = new google.maps.Marker({
            position : new google.maps.LatLng(-6.918782, 107.603329),
            title : 'lokasi',
            map : map,
            draggable : true
        });
        
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

        autocomplete.addListener('place_changed', function() {
            
            marker1.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }

            var address = '';
            if (place.address_components) {
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            marker1.setPosition(place.geometry.location);
            updateMarkerPosition(place.geometry.location, address);
            
            marker1.setVisible(true);

            
        });
    
        //updateMarkerPosition(latLng);

        google.maps.event.addListener(marker1, 'drag', function() {
            updateMarkerPosition(marker1.getPosition());
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpALWzkNO7VH_pCSX30bt43_7h3sIeqQI&libraries=places&callback=initMap" type="text/javascript"></script>  
</body>
</html>