<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNyLsAhFt4hIZKeNJYC244jPPayM0GhrY&sensor=false" type="text/javascript"></script>
<script type="text/javascript">
    document.getElementById('reset').onclick= function()
    {
        var field1= document.getElementById('lng');
        var field2= document.getElementById('lat');
        field1.value= field1.defaultValue;
        field2.value= field2.defaultValue;
    };
</script>    
<script type="text/javascript">
    function updateMarkerPosition(latLng) 
    {
      document.getElementById('lat').value = [latLng.lat()];
      document.getElementById('lng').value = [latLng.lng()];
    }

    var myOptions = 
    {
      zoom: 14,
      scaleControl: true,
      <?php foreach($konten as $data){?>
      center:  new google.maps.LatLng(<?=$data['lat']?>, <?=$data['lng']?>),
      <?php } ?> 
      mapTypeId: 'roadmap'
    };
 
    var map = new google.maps.Map(document.getElementById("map"),myOptions);

    var marker1 = new google.maps.Marker({
    <?php foreach($konten as $data){?>
    position : new google.maps.LatLng(<?=$data['lat']?>, <?=$data['lng']?>),
    <?php } ?>
    title : 'lokasi',
    map : map,
    draggable : true
    });
 
    //updateMarkerPosition(latLng);

    google.maps.event.addListener(marker1, 'drag', function() {
    updateMarkerPosition(marker1.getPosition());
    });
</script>  
</body>
</html>