

<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $this->load->view('error/index');
                ?>
                
                <?php if($this->session->flashdata('error_image')){ ?>
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Gagal upload image</h4>
                        <p><?= $this->session->flashdata('error_image') ?></p>
                    </div>
                <?php } ?>

                <div class="card">
                    <div id="map"></div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Deskripsi iklan</h4>
                        <h6 class="card-subtitle">Tambah iklan baru</h6>                       
                        <div class="table-responsive m-t-40">
                        <div class="basic-form">
                            <form action="<?= base_url('regis_store_tenant') ?>" method="post" enctype='multipart/form-data'>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-default" name="judul" placeholder="Judul Iklan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-default" name="alamat" id="alamat" placeholder="Alamat" required>
                                    </div>                                  
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-default" name="lat" id="lat" placeholder="Gerakan marker sesuai lokasi anda" required>
                                    </div>                                  
                                </div>
                                <div class="form-group">
                                <div class="col-md-6">
                                        <input type="text" class="form-control input-default" name="lng" id="lng" placeholder="Gerakan marker sesuai lokasi anda" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-default" name="keterangan"  placeholder="Keterangan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="">Gambar iklan</label>
                                        <input name="gambar" class="form-control input-default" type="file" required>
                                        <span style="font-size:12px;margin-top:20px;padding-top:20px;">ukuran image <= 1 MB </span>
                                    </div>
                                </div>
                                &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-info">Save</button>                  
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- End PAge Content -->

  
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        