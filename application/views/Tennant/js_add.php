<script type="text/javascript">
    function updateMarkerPosition(latLng) 
    {
      document.getElementById('lat').value = [latLng.lat()];
      document.getElementById('lng').value = [latLng.lng()];
    }

    function initMap() {
        
        var myOptions = 
        {
        zoom: 14,
        scaleControl: true,
        center:  new google.maps.LatLng(-6.918782, 107.603329),
        mapTypeId: 'roadmap'
        };
    
        var map = new google.maps.Map(document.getElementById("map"),myOptions);

        var marker1 = new google.maps.Marker({
            position : new google.maps.LatLng(-6.918782, 107.603329),
            title : 'lokasi',
            map : map,
            draggable : true
        });
    
        //updateMarkerPosition(latLng);

        google.maps.event.addListener(marker1, 'drag', function() {
            updateMarkerPosition(marker1.getPosition());
        });   
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpALWzkNO7VH_pCSX30bt43_7h3sIeqQI&callback=initMap" type="text/javascript"></script>  
</body>
</html>