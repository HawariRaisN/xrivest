
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            
            <div class="col-lg-12">
                
                <?php
                    $this->load->view('error/index');
                ?>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">List of <?= $s1 ?></h6>
                        <a href="<?= base_url('formAdd') ?>"><button type="button" class="btn btn-info">tambah iklan</button></a>
                        <div class="table-responsive m-t-40">
                        <table class="table table-bordered table-striped table-condensed flip-content" id="example">
                            <thead class="flip-content">
                              <tr>
                                  <td>No</td>
                                  <td>Nama Perusahaan</td>
                                  <td>Judul</td>
                                  <td>Keterangan</td>
                                  <td>Gambar</td>
                                  
                                  <td>Aksi</td>
                              </tr>
                            </thead>
                            <tbody id="myTable">
                                <?php $no=1; foreach ($konten as $isi) { ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $isi['nama'] ?></td>
                                        <td><?= $isi['judul'] ?></td>
                                        <td><?= $isi['keterangan'] ?></td>
                                        <td><img src="<?= $isi['gambar'] ?>" width="50" height="50" /></td>
                                        <!--<td><?= $isi['lat'] ?></td>-->
                                        <!--<td><?= $isi['lng'] ?></td>-->
                                        <td><a href="<?= site_url('tenant/up/'.$isi['id_iklan'])?>">Edit</a> | <a href="<?= site_url('tenant/del/'.$isi['id_iklan'])?>">Delete</a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                  <td>No</td>
                                  <td>Nama Perusahaan</td>
                                  <td>Judul</td>
                                  <td>Keterangan</td>
                                  <td>Gambar</td>
                                  <!--<td>Latitude</td>-->
                                  <!--<td>Longitude</td>-->
                                  <td>Aksi</td>
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
        <!-- Modal -->  
        <div class="row">
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Form <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                    <table class="table">
                        <tr><td>Type</td><td><span id="type"></span></td></tr>
                        <tr><td>Name</td><td><span id="nameee"></span></td></tr>
                        <tr><td>Contact</td><td><span id="contact"></span></td></tr>
                        <tr><td>Family Contact</td><td><span id="contact2"></span></td></tr>
                        <tr><td>Driver's Vehicle  plate</td><td><span id="dvp"></span></td></tr>
                        <tr><td>Rivest ID Number</td><td><span id="rin"></span></td></tr>
                        <tr><td>Location</td><td><span id="location"></span></td></tr>
                        <tr><td>Date</td><td><span id="date"></span></td></tr>
                        <!-- <form role="form" id="form-validation1" action="#" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="ssid_m" id="ssid_m" class="form-control input-md" placeholder="SSID Name" tabindex="1" data-error="SSID must be entered" required></div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="status_m" id="status_m" class="form-control input-md" placeholder="Sudah/Belum Aktiv" tabindex="2" data-error="Status must be entered" required>
                                        <input type="hidden" name="id_m" id="id_m">
                                    </div>
                                </div>
                            </div>                                
                            
                        </form> -->
                        <!-- <tr>
                            <td>Chronology</td>
                            <td> 
                                <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                            </td>
                        </tr> -->
                    </table>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        
                    </div>

                    </div>
                </div>
            </div>
            <div class="modal" id="myModal2">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Chronology <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form role="form" id="form-validation1" action="#" method="post">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <table>
                                                <tr>
                                                    <td> <label for="Chronology"> Chronology</label></td>
                                                    <td>:</td>
                                                    <td><textarea name="keterangan" id="keterangan" class="form-control" style="height:150px;width:300px;" required></textarea></td>
                                                    <input type="hidden" name="id_m" id="id_m">
                                                </tr>
                                            </table>   
                                        </div>
                                    </div>
                                    
                                </div>                                
                                
                        </form>
                    </div>
                    

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button class="btn btn-success" data-dismiss="modal" onclick="actionEditChronology()" id="update">Update</button>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Container fluid  -->
    
</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        