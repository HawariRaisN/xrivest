    <script src="<?php echo base_url('src/backend/'); ?>js/lib/datatables/datatables.min.js"></script>
  


    <script>
        $(document).ready(function() {
           
        } );
    </script>

    <script src="https://www.gstatic.com/firebasejs/4.12.0/firebase.js"></script>
    <script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyB4Ddoz4G4P9KnjLrSI8XF5R0vqm3M4qB0",
        authDomain: "rivest-4507.firebaseapp.com",
        databaseURL: "https://rivest-4507.firebaseio.com",
        projectId: "rivest-4507",
        storageBucket: "rivest-4507.appspot.com",
        messagingSenderId: "1062631027"
    };
    firebase.initializeApp(config);

    var db = firebase.database();
    var kecelakaanRef = db.ref('pengguna');

    // ---show data----
    kecelakaanRef.orderByChild('id_pengguna').on('value', showData, showError);
    function showData(data) {
        var _table = document.getElementById('myTable');
        var _tambah = '';
        var no = 1;

        data.forEach(function(child){
            _tambah += "<tr>" 
                            +"<td>" + no + "</td>" 
                            +"<td>" + child.val().nama_pengguna + "</td>" 
                            +"<td>" + child.val().alamat_pengguna + "</td>" 
                            +"<td>" + child.val().kontak_pengguna + "</td>" 
                            +"<td>" + child.val().kerabat_pengguna + "</td>"
                            +"<td><a href='#' onclick='detailPengguna(\"" +child.key+ "\")' data-toggle='modal' data-target='#myModal'><i class='fa fa-search-plus' title='Detail'></i></a> </td>" 
                        +"</tr>";
            no++;
        })
        _table.innerHTML=_tambah;
        $('#example').DataTable();  
    }

    function showError(err) {
        console.log(err.val())
    }
    // ---show data----

    function detailPengguna(id) {
        kecelakaanRef.child(id).once('value', function(datanya){
            $("#nameee").html(datanya.val().nama_pengguna)
            $("#dob").html(datanya.val().tanggal_pengguna)
            $("#gender").html(datanya.val().kelamin_pengguna)
            $("#address").html(datanya.val().alamat_pengguna)
            $("#job").html(datanya.val().pekerjaan_pengguna)
            $("#contact").html(datanya.val().kontak_pengguna)
            $("#contact2").html(datanya.val().kerabat_pengguna)
            $("#email").html(datanya.val().email_pengguna)
            $("#rdi").html(datanya.val().alat_pengguna)
            $("#dvp").html(datanya.val().kendaraan_pengguna)
            $("#dl").html(datanya.val().sim_pengguna)
        })
    }
    
    </script>

</body>
</html>