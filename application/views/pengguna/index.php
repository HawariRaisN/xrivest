
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">List of <?= $s1 ?></h6>
                        <div class="table-responsive m-t-40">
                        <table class="table table-bordered table-striped table-condensed flip-content" id="example">
                        <thead class="flip-content">
                            <tr>
                                <td>No</td>
                                <td>Name</td>
                                <td>Address</td>
                                <td>Contact</td>
                                <td>Family Contact</td>
                                <td>Detail</td>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>No</td>
                                <td>Name</td>
                                <td>Address</td>
                                <td>Contact</td>
                                <td>Family Contact</td>
                                <td>Detail</td>
                            </tr>
                        </tfoot>
                    </table>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- End PAge Content -->
        <div class="row">
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Form <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                    <table class="table">
                        
                        <tr><td>Name</td><td><span id="nameee"></span></td></tr>
                        <tr><td>Date Of Birth</td><td><span id="dob"></span></td></tr>
                        <tr><td>Gender</td><td><span id="gender"></span></td></tr>
                        <tr><td>Address</td><td><span id="address"></span></td></tr>
                        <tr><td>Job</td><td><span id="job"></span></td></tr>
                        <tr><td>Contact</td><td><span id="contact"></span></td></tr>
                        <tr><td>Family Contact</td><td><span id="contact2"></span></td></tr>
                        <tr><td>Email</td><td><span id="email"></span></td></tr>
                        <tr><td>Rivest Devices ID</td><td><span id="rdi"></span></td></tr>
                        <tr><td>Driver's Vehicle  plate</td><td><span id="dvp"></span></td></tr>
                        <tr><td>Driver's license</td><td><span id="dl"></span></td></tr>
                       
                    </table>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Container fluid  -->
    
</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        