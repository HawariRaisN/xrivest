
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div id="map"></div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">Add Of <?= $s1 ?></h6>                       
                        <div class="table-responsive m-t-40">
                        <div class="basic-form">
                        <?php foreach($konten as $data){?>
                        <form action="<?= base_url('ps/au') ?>" method="post">
                        <input type="hidden" name="id_pos" value="<?php echo $data['id_pos']; ?>">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control input-default" value="<?=$data['nama_lokasi']?>" name="nama_lokasi" placeholder="Nama Lokasi" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control input-default" value="<?=$data['lat']?>" name="lat" id="lat" placeholder="Lattitude" required>
                                </div>                                  
                            </div>
                            <div class="form-group">
                            <div class="col-md-6">
                                    <input type="text" class="form-control input-default" name="lng" value="<?=$data['lng']?>" id="lng" placeholder="Longitude" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control input-default" name="keterangan" value="<?=$data['keterangan']?>"  placeholder="Keterangan" required>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-info">Save</button>                  
                        </form>
                        <?php } ?>        
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->

  
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        