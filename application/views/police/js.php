<script src="<?php echo base_url('src/backend/'); ?>js/lib/datatables/datatables.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpALWzkNO7VH_pCSX30bt43_7h3sIeqQI&libraries=places&callback=initMap" async defer></script>
        
<script>
           

function initMap() {

  var infoWindow = new google.maps.InfoWindow;
  var bounds = new google.maps.LatLngBounds(); 
  
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: -6.885500, lng: 107.615407},
    mapTypeId: 'roadmap'
  });


    <?php foreach($konten as $data){
            
        $nama = $data['nama_lokasi'];
        $lat = $data['lat'];
        $lon = $data['lng'];
        $ket = $data['keterangan'];
        $image= base_url('src/marker/pospolisi.png');

        echo ("addMarker($lat, $lon,'$image', 'Nama : $nama<br/>Keterangan : $ket');\n");                                  
    } ?>

function addMarker(lat, lng, img, info) {
    var lokasi = new google.maps.LatLng(lat, lng);
    bounds.extend(lokasi);
    var marker = new google.maps.Marker({
        map: map,
        position: lokasi,
        icon: img
    });       
    map.fitBounds(bounds);
    bindInfoWindow(marker, map, infoWindow, info);
  }

  function bindInfoWindow(marker, map, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function() {
      infoWindow.setContent(html);
      infoWindow.open(map, marker);
    });
  }


}

</script>

</body>
</html>