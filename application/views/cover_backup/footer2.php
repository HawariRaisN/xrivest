</aside>
        <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="<?php echo base_url('src/backend/') ;?>js/jquery-1.11.3.min.js" type="text/javascript"> </script>
    <script src="<?php echo base_url('src/backend/') ;?>js/bootstrap.min.js" type="text/javascript"></script>
    <!--livicons-->
    <script src="<?php echo base_url('src/backend/');?>vendors/livicons/minified/raphael-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('src/backend/');?>vendors/livicons/minified/livicons-1.4.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url('src/backend/') ;?>js/josh.js" type="text/javascript"></script>
    <script src="<?php echo base_url('src/backend/') ;?>js/metisMenu.js" type="text/javascript"> </script>
    <script src="<?php echo base_url('src/backend/');?>vendors/holder/holder.js" type="text/javascript"></script>
    <!-- end of global js -->
