
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">List of <?= $s1 ?></h6>
                        <button type="button" class="btn btn-info" data-toggle='modal' data-target='#myModal'>Add</button>
                        <div class="table-responsive m-t-40">
                        <table class="table table-bordered table-striped table-condensed flip-content" id="example">
                        <thead class="flip-content">
                            <tr>
                                <td>No</td>
                                <td>Name</td>
                                <td>Username</td>
                                <td>Role</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($data_root as $su)
                                {
                                ?>
                            <tr>
                              <td width="80px"><?php echo ++$start ?></td>
                              <td><?= $su->nama ?></td>
                              <td><?= $su->username ?></td>
                              <td><?= $su->role ?></td>
                              <td> <a href="<?= site_url('su/del/'.$su->id)?>">Delete</a></td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>No</td>
                                <td>Name</td>
                                <td>Username</td>
                                <td>Role</td>
                                <td>Aksi</td>
                            </tr>
                        </tfoot>
                    </table>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- End PAge Content -->

        <div class="row">
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Form <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form action="<?= site_url('su/a'); ?>" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="nama" id="nama" class="form-control input-md" placeholder="Full Name" tabindex="1" data-error="SSID must be entered" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <!-- <input type="text"  placeholder="Role" tabindex="2" > -->
                                        <select name="role" id="role" class="form-control input-md" data-error="Status must be entered" required>
                                            <option value="">--Pilih Role--</option>
                                            <option value="Admin">Admin</option>
                                            <option value="Police">Police</option>
                                            <option value="JasaRaharja">JasaRaharja</option>
                                            <option value="Dishub">Dishub</option>
                                            <option value="Tennant">Tennant</option>
                                        </select>
                                        <!-- <input type="hidden" name="id_m" id="id_m"> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" class="form-control input-md" placeholder="Username" tabindex="2" data-error="Status must be entered" autocomplete="off" required>
                                        <!-- <input type="hidden" name="id_m" id="id_m"> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-md" placeholder="Password" tabindex="2" data-error="Status must be entered" required>
                                        <!-- <input type="hidden" name="id_m" id="id_m"> -->
                                    </div>
                                </div>
                            </div>                                
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Container fluid  -->
    
</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        