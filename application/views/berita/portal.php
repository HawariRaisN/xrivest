<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('src/backend/'); ?>images/favicon.ico">
    <title>Rivest | Berita</title>

    <style>
      #terget{
        text-decoration:none;
        font-size: 20px;
      }
    </style>
  </head>
  <body>
    <!-- Image and text -->
    <section class="jumbotron text-center">
        
    <main role="main">
      <div class="album py-5 bg-light">
        <div class="container">
          <div class="row">
            <?php 

            function namahari($tanggal){

              $tgl=substr($tanggal,8,2);
              $bln=substr($tanggal,5,2);
              $thn=substr($tanggal,0,4);

              $info=date('w', mktime(0,0,0,$bln,$tgl,$thn));
              
              switch($info){
                  case '0': return "Minggu"; break;
                  case '1': return "Senin"; break;
                  case '2': return "Selasa"; break;
                  case '3': return "Rabu"; break;
                  case '4': return "Kamis"; break;
                  case '5': return "Jumat"; break;
                  case '6': return "Sabtu"; break;
              };
                
            }

            function pecah_tanggal($tanggal, $kondisi)
            {
                if($kondisi == "waktu"){
                   $expload = explode(" ",$tanggal);

                   return $expload[1];

                }else if($kondisi == "tanggal_pecah"){
                    $data = [];
                    $expload = explode("-", $tanggal);
                    $data[0] = $expload[0];

                    switch ($expload[1]) {      
                      case '01' : $bulan = 'Januari';break;
                      case '02' : $bulan = 'Februari';break;
                      case '03' : $bulan = 'Maret';break;
                      case '04' : $bulan = 'April';break;
                      case '05' : $bulan = 'Mei';break;
                      case '06' : $bulan = 'Juni';break;
                      case '07' : $bulan = 'Juli';break;
                      case '08' : $bulan = 'Agustus';break;
                      case '09' : $bulan = 'September';break;
                      case '10' : $bulan = 'Oktober';break;
                      case '11' : $bulan = 'November';break;
                      case '12' : $bulan = 'Desember';break;
                    }

                    $data[1] = $bulan;
                    $get_hari = explode(" ", $expload[2]);
                    $data[2] = $get_hari[0];

                    return $data;
                }
            }
            
            foreach ($konten as $data) { 
              
              $get_dat = pecah_tanggal($data['tanggal'], "tanggal_pecah");
              
              ?>
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top" src="<?= $data['gambar'] ?>" alt="Card image cap">
                  <div class="card-body">
                    <p class="card-text"><a id="terget" href="<?= site_url('and/detail_berita/'.$data['slug']); ?>"><?= $data['judul_berita'] ?></a></p>
                    <div class="d-flex justify-content-between align-items-center">
                      <small class="text-muted"><?= namahari($data['tanggal']) ?>, <?= $get_dat[2].' '.$get_dat[1].' '. $get_dat[0]?></small>
                      <small class="text-muted"><?= pecah_tanggal($data['tanggal'],"waktu") ?> WIB</small>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </main>
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>