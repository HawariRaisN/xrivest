
<!-- Page wrapper  -->
<link rel="stylesheet" href="<?= base_url('assets/') ?>datetimepicker-master/build/jquery.datetimepicker.min.css">
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">Add Of <?= $s1 ?></h6>                       
                        <div class="table-responsive m-t-40">
                        <div class="basic-form">
                        	<?php foreach($konten as $data){?>
                            <form action="<?= base_url('proses_update') ?>" method="post" enctype='multipart/form-data'>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-default" name="judul_berita" placeholder="Judul Iklan" value="<?=$data['judul_berita']?>" required>
                                        <input type="hidden" name="id_berita" value="<?= $data['id_berita'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <div class='input-group'>
                                            <input type='text' id="datetimepicker" value="<?= $data['tanggal'] ?>" class="form-control" name="tanggal" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <textarea rows="5" cols="10" class="form-control" name="des_berita"><?= $data['des_berita'] ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="">Gambar berita</label>
                                        <input name="gambar" class="form-control input-default" type="file">
                                        <span style="font-size:12px;margin-top:20px;padding-top:20px;">ukuran image <= 1 MB </span>
                                    </div>
                                </div>
                                &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-info">Update</button>                  
                            </form>
                            <?php } ?> 
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= base_url('assets/') ?>datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>
    jQuery('#datetimepicker').datetimepicker({
        format:'Y-m-d H:i'
    });
    tinymce.init({ selector:'textarea' });
</script>
        