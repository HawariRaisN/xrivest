<footer class="footer"> © 2018 All rights reserved. Edited From Colorlib</a></footer>
</div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <!--  -->
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('src/backend/'); ?>js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('src/backend/'); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('src/backend/'); ?>js/custom.min.js"></script>

    <script src="<?php echo base_url('src/backend/'); ?>js/lib/toastr/toastr.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/toastr/toastr.init.js"></script>

    <?php if ($this->session->flashdata('message')): ?>
    <script>
       $(function () {
        var data = '<?= $this->session->flashdata('message'); ?>';
         toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "rtl": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": 300,
          "hideDuration": 1000,
          "timeOut": 5000,
          "extendedTimeOut": 1000,
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        toastr.success(data);
      });
    </script>
  	<?php endif ?>


  	<?php if ($this->session->flashdata('error')): ?>
      <script>
         $(function () {
          var data = '<?= $this->session->flashdata('error'); ?>';
           toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "rtl": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 1000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
          toastr.error(data);
        });
      </script>
  	<?php endif ?>


</body>

</html>