<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('src/backend/'); ?>images/favicon.ico">
    <title>Rivest | Dashboard </title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('src/backend/'); ?>css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('src/backend/'); ?>css/helper.css" rel="stylesheet">
    <link href="<?php echo base_url('src/backend/'); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('src/maps/') ;?>css/map.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/iziToast/') ;?>dist/css/iziToast.min.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <link rel="manifest" href="<?php echo base_url('manifest.json'); ?>">
    <link href="<?php echo base_url('src/backend/'); ?>css/lib/toastr/toastr.min.css" rel="stylesheet">

</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you canh find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b><img src="<?php echo base_url('src/logo/'); ?>RivestLogo.png" alt="homepage" class="dark-logo" style="height:24px; width:24px;" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><img src="<?php echo base_url('src/logo/'); ?>RivestHeader@4x.png" alt="homepage" class="dark-logo" style="height:24px; width:70px;" /></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- Messages -->
                       
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- Search -->
                       
                        <!-- Comment -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i>
								<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle m-r-10"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>This is title</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-success btn-circle m-r-10"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>This is another title</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-info btn-circle m-r-10"><i class="ti-settings"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>This is title</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-primary btn-circle m-r-10"><i class="ti-user"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>This is another title</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- End Comment -->
                        
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url('src/logo/'); ?>RivestLogo.png" alt="user" class="profile-pic" style="height:24px; width:24px;" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a><i class="ti-user"></i>  <?php echo $this->session->userdata('nama'); ?></a></li>
                                    <li><a href="<?= site_url('logout'); ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        
                        <?php if($this->session->userdata('role') == 'Tennant'){ ?>
                            <?php 
                                $id = $this->session->userdata('id');
                                $query = $this->db->get_where('des_tenant', array('id_tenant' => $id))->row();

                                $st = $query->status_tenant;
                            ?>
                            <span class="badge badge-<?= $st == 'trailer' ? 'danger' : 'success' ?>" style="margin-left:0px;font-size:15px;padding:8px;width:100%;">Akun status <?= $st == 'trailer' ? 'Trial' : 'Premium' ?> </span>
                        <?php } ?>

                        <li class="nav-label">Home</li>
                        <li> <a href="<?= site_url('dashboard') ?>" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</a>
                        </li>
                        <li class="nav-label">Data Master</li>
                        <?php 
                            if ($this->session->userdata('role') == 'Admin') { ?>
                                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">Users</span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <!-- <li><a href="<?= site_url('p') ?>">Polices</a></li> -->
                                        <li><a href="<?= site_url('r') ?>">Rivest Users</a></li>
                                        <li><a href="<?= site_url('su') ?>">Super Users</a></li>
                                    </ul>
                                </li>
                                <li> <a href="<?= site_url('rd') ?>" aria-expanded="false"><i class="fa fa-wrench"></i><span class="hide-menu">Rivest Devices</span></a>
                                </li>
                        <?php    }
                      
                            if ($this->session->userdata('role') == 'Police') { ?>
                                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-map-marker"></i><span class="hide-menu">Maping</span></a>
                                            <ul aria-expanded="false" class="collapse">
                                                <li><a href="<?= site_url('ps') ?>">Polices Station</a></li>
                                                <li><a href="<?= site_url('rz') ?>">Red Zone</a></li>
                                            </ul>
                                        </li>
                                        <li class="nav-label">Berita</li>
                                        <li>
                                            <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bullhorn"></i><span class="hide-menu">Rivest Berita</span></a>
                                            <ul aria-expanded="false" class="collapse">
                                                <li><a href="<?= site_url('tambah_berita')?>">Tambah Berita</a></li>
                                                <li><a href="<?= site_url('tampil_berita')?>">Semua Berita</a></li>
                                            </ul>
                                        </li>  
                                        <li class="nav-label">Monitoring</li>
                                        <li> <a href="<?= site_url('k') ?>" aria-expanded="false"><i class="fa fa-map-marker"></i><span class="hide-menu">Rivest Monitoring </span></a>
                                        </li>
                                        <li class="nav-label">Reports</li>
                                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bar-chart"></i><span class="hide-menu">Accidents</span></a>
                                            <ul aria-expanded="false" class="collapse">
                                                <li><a href="<?= site_url('k/jr')?>">Chronology</a></li>
                                                <li><a href="<?= site_url('k/dcs')?>">Decisions</a></li>
                                            </ul>
                                        </li>
                         <?php  }
                      
                            if ($this->session->userdata('role') == 'JasaRaharja') 
                                { ?>
                                    <li class="nav-label">Monitoring</li>
                                    <li> <a href="<?= site_url('k') ?>" aria-expanded="false"><i class="fa fa-map-marker"></i><span class="hide-menu">Rivest Monitoring </span></a>
                                    </li>
                                    <li class="nav-label">Reports</li>
                                    <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-bar-chart"></i><span class="hide-menu">Accidents</span></a>
                                        <ul aria-expanded="false" class="collapse">
                                            <li><a href="<?= site_url('k/jsrhrj')?>">Approve By Polices</a></li>
                                        </ul>
                                    </li>
                        <?php }

                            if ($this->session->userdata('role') == 'Tennant') { ?>
                                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-map-marker"></i><span class="hide-menu">Iklan</span></a>
                                    <ul aria-expanded="false" class="collapse">
                                       
                                        <li><a href="<?= site_url('regis') ?>">Pasang Iklan</a></li>
                                    </ul>
                                </li>
                        <?php  }
                            ?>
                        
                       

                     

                        
						
                       
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        
    