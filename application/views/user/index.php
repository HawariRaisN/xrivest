
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Rivest | Home </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Interface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
	<meta property="og:url" content="http://myrivest.net/" />
    <meta property="og:title" content="MYRIVEST" />
    <meta property="og:description" content="Kami adalah Startup yang fokus pada pengembangan sistem cerdas untuk mendukung keselamatan berkendara" />
    <meta property="og:image" content="http://myrivest.net//src/logo/RivestLogo+slogan.png" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('src/backend/'); ?>images/favicon.ico">
    <!-- Custom Theme files -->
    <link href="<?= base_url(); ?>/src/frontend/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="<?= base_url(); ?>/src/frontend/css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- our capabilities -->
    <link href="<?= base_url(); ?>/src/frontend/css/timeline.css" type="text/css" rel="stylesheet" media="all">
    <!-- courses -->
    <link href="<?= base_url(); ?>/src/frontend/css/gridview.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/src/frontend/css/background_image.css">
    <!-- font-awesome icons -->
    <link href="<?= base_url(); ?>/src/frontend/css/font-awesome.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url(); ?>/src/frontend/css/carousel.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        #quote-carousel {
    padding: 0 10px 30px 10px;
    margin-top: 60px;
}
#quote-carousel .carousel-control {
    background: none;
    color: #CACACA;
    font-size: 2.3em;
    text-shadow: none;
    margin-top: 30px;
}
#quote-carousel .carousel-indicators {
    position: relative;
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-top: 20px;
    margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
    width: 50px;
    height: 50px;
    cursor: pointer;
    border: 1px solid #ccc;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    border-radius: 50%;
    opacity: 0.4;
    overflow: hidden;
    transition: all .4s ease-in;
    vertical-align: middle;
}
#quote-carousel .carousel-indicators .active {
    width: 128px;
    height: 128px;
    opacity: 1;
    transition: all .2s;
}
.item blockquote {
    border-left: none;
    margin: 0;
}
.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
}
    </style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- banner -->
    <div class="banner">
        <!-- header -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light navbar-fixed-top">
            <div class="container">
                <h1 class="wthree-logo">
                    <a href="index.html" id="logoLink" data-blast="color">Rivest</a>
                </h1>
                <div class="nav-sec  position-relative">
                    <a href="#menu" id="toggle">
                        <span></span>
                    </a>
                    <div id="menu">
                        <ul>
                            <li class="active"><a href="index.html">Home</a></li>
                            <li><a class="scroll" href="#tentang">Tentang</a></li>
                            <li><a class="scroll" href="#produk">Produk</a></li>
                            <li><a class="scroll" href="#faq">FAQ</a></li>
                            <li><a href="regis_tenant">Tenant</a></li>
                            <li><a class="scroll" href="#testimonial">Testimonial</a></li>
                            <li><a class="scroll" href="#kontak">Kontak</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- //header -->
        <!-- banner -->
        <div class="container">
          <div class="row">
            <div class="col-sm">
              
            </div>
            <div class="col-sm">
              
            </div>
            <div class="col-sm">
              <div class="container">
            <div class="row banner-row">
                <div class="col-lg-4 text-center">
                    <div class='backbox'>
                        <h4 class="bb-text">RIVEST<br> Smart Driving System Berbasis Internet Of Things .</h4>
                    </div>
                </div>
            </div>
        </div>
            </div>
          </div>
        </div>
    </div>
    <!-- //banner -->
    <!-- services  -->
    <div class="banner1">
    <section class="servicesw3-top py-5" id="tentang">
        <div class="container py-lg-5">
            <div class="title-sec-agile text-center">
                <span class="title-icon-wthree fa fa-hourglass-o" aria-hidden="true"></span>
                <h3 class="agile-head">Tentang Team</h3>
            </div>
            <!-- services top row -->
            <!-- Main jumbotron for a primary marketing message or call to action -->
              <div class="jumbotron">
                <div class="container"  style="text-align:center">
                  <p>Kami adalah Startup yang fokus pada pengembangan sistem cerdas untuk mendukung keselamatan berkendara</p>
                  <!-- Go to www.addthis.com/dashboard to customize your tools -->
                  
                  <!-- <p><a class="btn btn-outline-info" href="#" role="button">Informasi selengkapnya &raquo;</a></p> -->
                </div>
              </div>
            <!-- //services top row -->
        </div>
    </section>
    
    <!-- services -->
    <!-- grid view -->
    <!-- <div style="background-color: #28b6f6;"> -->
        <section class="courses-sec py-lg-5 pb-5" id="produk">
            <div class="container">
                <div class="title-sec-agile text-center">
                    <span class="title-icon-wthree fa fa-hourglass-o" aria-hidden="true"></span>
                    <h3 class="agile-head">Produk Kami</h3>
                </div>
                <div class="row py-3">
                    <div class="col-lg-12 my-3">
                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-info" id="list">
                                    <i class="fa fa-list-ul"></i>
                                </button>
                                <button class="btn btn-danger" id="grid">
                                    <i class="fa fa-th"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="products" class="row view-group">
                    <div class="item col-lg-4">
                        <div class="thumbnail card" style="height: 770px;">
                            <div class="img-event">
                                <img class="group list-group-image img-fluid" src="<?= base_url(); ?>/src/frontend/images/device2.png" alt="" />
                            </div>
                            <div class="caption card-body" >
                                <h4 class="group card-title inner list-group-item-heading">
                                    Rivest Devices</h4>
                                <p class="group inner list-group-item-text">
                                    <p2>Deskripsi produk :</p>
                                    <li>Tersedia dalam 2 model, wearable (rompi/jaket) dan portable smart button</li>
                                    <li>Dilengkapi beberapa sensor untuk mendeteksi posisi duduk pengendara (normal/terjatuh)</li>
                                    <li>Untuk model wearable dilengkapi dengan panic button</li>
                                    <li>Menggunakan baterei lithium yang dapat diisi ulang</li>
                                    <li>Konfigurasi dan aktivasi perangkat menggunakan Rivest Apps </li>
                                    <li>Penempatan perangkat dapat disesuaikan dengan jaket/rompi yang beragam</li>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="item col-lg-4">
                        <div class="thumbnail card">
                            <div class="img-event">
                                <img class="group list-group-image img-fluid" src="<?= base_url(); ?>/src/frontend/images/tombol_portable_1.jpg" alt="" />
                            </div>
                            <div class="caption card-body" style="width: 18rem; height: 330px;">
                                <h4 class="group card-title inner list-group-item-heading">
                                    Tombol Portable</h4>
                                <p class="group inner list-group-item-text">
                                    <li>Mendeteksi posisi pengguna Rivest</li>
                                    <li>Panic Button</li>
                                    <li>Power batre perangkat Rivest</li>
                                    <li>Perangkat berbasis Internet Of Thing’s</li>
                                    <li>Konfigurasi dinamis melalui interface website</li>
                                </p>
                            </div>
                        </div>
                    </div> -->
                    <div class="item col-lg-4">
                        <div class="thumbnail card" style="height: 770px;">
                            <div class="img-event" align="center">
                                <img class="group list-group-image img-fluid" src="<?= base_url(); ?>/src/frontend/images/apps2.png" alt="" />
                            </div>
                            <div class="caption card-body">
                                <h4 class="group card-title inner list-group-item-heading">
                                    Android Apps</h4>
                                <p class="group inner list-group-item-text">
                                    <p2>Deskripsi produk :</p>
                                    <li>Untuk registrasi dan aktivasi perangkat Rivest </li>
                                    <li>Dilengkapi peta digital untuk info pos polisi, patroli polisi dalam radius tertentu</li>
                                    <li>Notifikasi ketika masuk daerah rawan bencana, rawan kecelakaan dan rawan kejahatan</li>
                                    <li>Info mengenai kondisi jalan terkini agar pengguna dapat memilih jalur yang tepat</li>
                                    <li>Info mitra (tenant) pada daerah tertentu</li>
                                    <li>Tersedia mode berkendara sendiri dan mode group (touring)</li>
                                    <li>Dilengkapi Text to Speech</li>
                                    <li>Dilengkapi Log bagi pengendara</li>
                                    <li>Kontak darurat ke kepolisian, ambulans, dan kerabat/keluarga</li>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item col-lg-4">
                        <div class="thumbnail card" style="height: 770px;">
                            <div class="img-event">
                                <img class="group list-group-image img-fluid" src="<?php echo base_url(); ?>/src/frontend/images/webs.png" alt="" />
                            </div>
                            <div class="caption card-body">
                                <h4 class="group card-title inner list-group-item-heading">
                                    Rivest Cloud</h4>
                                <p class="group inner list-group-item-text">
                                    <p2>Deskripsi produk :</p>
                                    <li>Manajemen data terpadu untuk Rivest Devices, pengguna dan mitra Rivest</li>
                                    <li>Pemetaan pos polisi dan polisi yang sedang berpatroli secara dinamis</li>
                                    <li>Penerapan Geo Fence untuk daerah rawan bencana, rawan kecelakaan dan rawan kejahatan</li>
                                    <li>Notifikasi bagi kepolisian, Dinas perhubungan ketika terjadi kecelakaan</li>
                                    <li>Rekapitulasi kecelakaan bagi pihak kepolisian (per-wilayah, per-waktu)</li>
                                    <li>E-BAP untuk pencatatan kejadian kecelakaan</li>
                                    <li>Dashboard  bagi kepolisian dan Dishub memberikan informasi</li>
                                    <li>Dashboard untuk mitra (tenant) untuk mempromosikan produknya</li>
                                    <li>Data analytic pengguna berbasis machine learning</li>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- //grid view -->
     <!-- FAQ -->
    <section class="servicesw3-top py-5" id="faq">
        <div class="container py-lg-5">
            <!-- services top row -->
            <!-- Main jumbotron for a primary marketing message or call to action -->
              <div class="jumbotron">
                <div class="container"  style="text-align:center">
                  <p>
                    <H4>FAQ</H4>
                      <div id="accordion">
                      <div class="card">
                        <div class="card-header" id="headingOne">
                          <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Apakah produk rivest hanya untuk roda dua (Sepeda Motor) ?
                            </button>
                          </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                          <div class="card-body">
                            Produk Rivest dapat digunakan untuk keadaan roda dua maupun untuk roda empat, namun saat ini kami fokus untuk kendaraan roda dua dalam hal ini sepeda motor.
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="headingTwo">
                          <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Apakah produk rivest dijual bebas ?
                            </button>
                          </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                          <div class="card-body">
                            Untuk perangkat dijual dengan harga tertentu melalui distributor, sedangkan aplikasi android diberikan secara gratis dan dapat di download di googleplay.
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="headingThree">
                          <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Bagaimana cara bermitra dengan rivest ?
                            </button>
                          </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                          <div class="card-body">
                            Dengan cara mengisi form di bawah sini.
                          </div>
                        </div>
                      </div>
                    </div>
                  </p>
                  <!-- Go to www.addthis.com/dashboard to customize your tools -->
                  
                  <!-- <p><a class="btn btn-outline-info" href="#" role="button">Informasi selengkapnya &raquo;</a></p> -->
                </div>
              </div>
            <!-- //services top row -->
        </div>
    </section>
<!-- /FAQ -->
    <!-- our capabilities -->
        <div class="banner2">
            <section class="processw3-top py-5" id="testimonial">
                <div class="container py-md-5">
                    <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <div class="title-sec-agile text-center">
                                <span class="title-icon-wthree fa fa-hourglass-o" aria-hidden="true"></span>
                                <h3 class="agile-head">Testimonial</h3>
                            </div>
                            <!-- Carousel Slides / Quotes -->
                            <div class="carousel-inner text-center">
                                
                                <div class="item active">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <p>Mengenai alat keselamatan berkendara ini, sangat bagus karena ada peringatan dini bagi pengendara untuk keselamatannya. </p>
                                                <small>Didit (Pegawai Dinas Provinsi Jawa Barat)</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                  
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <p>Setelah tadi saya perhatikan produk yang disampaikan saya sangat tertarik insyaallah nanti kita bantu kerjasama dengan dinas perhubungan.</p>
                                                <small>Yudi K wahyu (Humas Dinas Perhubungan Jawa Barat)</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                             
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <p>Menurut kami ini merupakan sebuah inovasi bagi pihak pengendara dan pihak kepolisian, tentu dengan adanya sistem pemantau ini, kami akan lebih mudah untuk mengetahui lokasi terjadinya potensi tindak kejahatan atau kecelakaan, agar tim kami dapat segera ke TKP untuk menanganinya.</p>
                                                <small>Tomy Yunatan (Kanit Binmas Polsek Bojong Soang)</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <p>Sangat bermanfaat produk Rivest ini, karena membantu dalam berkendara, tidak hanya sebagai pelindung badan, namun berguna ketika akan terjadi potensi kejahatan atau pengendara mengalami kecelakaan, terutama dengan memberikan informasi ke kerabat dan pihak kepolisian.</p>
                                                <small>Hendra Setiawan, M.Pd (Biker, Dosen Universitas Telkom)</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <p>Bagi petugas kesehatan, perlu untuk mengetahui kecepatan berkendara dan posisi jatuh pengendara saat terjadi kecelakaan, Hal ini bertujuan untuk memudahkan kami dalam identifikasi dan prioritas penanganan pasien, dengan adanya Rivest, kami pikir akan dapat membantu kami.</p>
                                                <small>Dea Guntur Rahayu, S.Ked</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                            </div>
                            <!-- Bottom Carousel Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="<?php echo base_url(); ?>/src/frontend/images/ada.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="<?php echo base_url(); ?>/src/frontend/images/wahyu.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="<?php echo base_url(); ?>/src/frontend/images/polisi.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="3"><img class="img-responsive" src="<?php echo base_url(); ?>/src/frontend/images/kuy.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="4"><img class="img-responsive" src="<?php echo base_url(); ?>/src/frontend/images/oo.jpg" alt="">
                                </li>
                            </ol>

                            <!-- Carousel Buttons Next/Prev -->
                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </section>
        </div>

    <!-- //our capabilities -->
    <!-- footer top -->
    <div class="footer-top py-5">
        <div class="container">
            <div class="row footer-cform pt-lg-5">
                <div class="col-lg-5 wthree-form-left my-lg-0 mt-4" id="kontak">
                    <h5 class="footer-top-title">Social Media</h5>
                    <div class="social-icons d-flex  mt-lg-5 my-5">
                        <h2 class="mr-4">stay connected :</h2>
                        <ul class="social-iconsv2 agileinfo" style="margin-top:3%">
                            <li>
                                <a href="https://instagram.com/rivest_official" target="_blank">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-6">
                    <h5 class="footer-top-title">site navigation</h5>
                    <div class="footer-top-agileits">
                        <ul class="list-agileits d-flex" style="margin-bottom:2px;">
                            <a href="">
                            <img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" style="width: 200px; height: 100px;"></a>
                        </ul>
                        <div style="margin-top:-5px;margin-left:15px;">
                            <p style="color:white;">Coming Soon</p>
                        </div> 
                        
                    </div>
                    <div class="footer-cont-btm">
                        <h5 class="footer-top-title">contact information</h5>
                        <address class="my-4">
                            <p>Jl. Dipati Ukur No.112-116, Lebakgede, Coblong, Kota Bandung, Jawa Barat 40132.</p>
                        </address>
                        <ul class="d-flex header-agile pt-0 flex-column">
                            <li>
                                <span class="fa fa-envelope-open mr-3"></span>
                                rivest.erg@gmail.com
                                <!-- <a href="rivest.erg@gmail.com">rivest.erg@gmail.com</a> -->
                            </li>
                            <li class="mt-3">
                                <span class="fa fa-phone mr-3"></span>
                                <p class="d-inline">082116871007</p>
                            </li>
                        </ul>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
    
    <!-- //footer top -->
    <!-- footer -->
    <!-- <footer>
        <div class="container">
            <div class="cpy-right text-center">
                <p class="text-white">© 2018 MyRivest</p>
            </div>
        </div>
    </footer> -->
    <!-- //footer -->
    <!-- Custom JavaScript for this theme -->
    <script type="text/javascript">
        var addthis_widget = {
              url: "http://myrivest.net/",
              title: "MYIVEST",
              description: "Kami adalah Startup yang fokus pada pengembangan sistem cerdas untuk mendukung keselamatan berkendara",
              media: "http://myrivest.net//src/frontend/images/ada.jpg"
            }
    </script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bd9c5a017bbb3d5"></script>
    <script src="<?php echo base_url(); ?>/src/frontend/js/scrolling-nav.js"></script>


    <script type="text/javascript">

      function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    } 

    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="<?php echo base_url(); ?>/src/frontend/js/carousel.js" type="text/javascript" charset="utf-8" async defer></script>
    <!-- js -->
    <script src="<?php echo base_url(); ?>/src/frontend/js/jquery-2.2.3.min.js"></script>
    <!-- //js -->
    <script src="<?php echo base_url(); ?>/src/frontend/js/gridview.js"></script>
    <!-- //grid view -->
    <!--  menu toggle -->
    <script src="<?php echo base_url(); ?>/src/frontend/js/menu.js"></script>
    <!-- Scrolling Nav JavaScript -->
    <script src="<?php echo base_url(); ?>/src/frontend/js/scrolling-nav.js"></script>
    <!-- start-smooth-scrolling -->
    <script src="<?php echo base_url(); ?>/src/frontend/js/move-top.js"></script>
    <script src="<?php echo base_url(); ?>/src/frontend/js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        $(document).ready(function () {
            
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
            };
            

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src="<?php echo base_url(); ?>/src/frontend/js/SmoothScroll.min.js"></script>
    <!-- //smooth-scrolling-of-move-up -->
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>/src/frontend/js/bootstrap.js"></script>
<div class="chating" style=" z-index: 99999; width: 50px; padding: 15px; left: 0; bottom: 0; position: fixed; "> <a href='https://api.whatsapp.com/send?phone=6282116871007&amp;text=Asalamulaikum Admin saya mau tanya-tanya produk Rivest' target='_blank'><img alt='Hubungi Kami' src='https://2.bp.blogspot.com/-qsp7D2BwbAg/W3LsSlnqIrI/AAAAAAAAKbo/giXulfdrpC8X9t1Hkttm4gKGC2nLI11WwCLcBGAs/s1600/wa.png' title='Hubungi Kami' align="left" style="width: 50px;"/></a></div>   
<script type="text/javascript">
    
</script>

</body>

</html>