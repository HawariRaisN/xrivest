<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Rivest | Tenant</title>
    
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('src/backend/'); ?>images/favicon.ico">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://fonts.googleapis.com/css?family=Raleway|Roboto" rel="stylesheet">
</head>
<style>
    img {
        width:100%;
    }
    .p-tenant{
        margin-top:70px;
        font-size:18px;
        font-weight: bold;
        font-family: 'Roboto', sans-serif;
        letter-spacing: 1px;
    }
</style>
<body>
<section class="testimonial py-5" id="testimonial">
    <div class="container">
        <div class="row ">
        
            <div class="col-md-4 py-5 bg-primary text-white text-center ">
                <div class=" ">
                    <div class="card-body">
                        <img src="http://www.ansonika.com/mavia/img/registration_bg.svg" style="width:30%">
                        <h2 class="py-3">Registrasi Tenant</h2>
                        <p>Daftarkan Iklanmu sekarang!</p>
                        
                        <a href="<?= site_url('rivest-login'); ?>" class="btn btn-outline-light bnt-sm">login</a>
                        <p class="p-tenant">"Dengan memasang iklan di aplikasi kami tenant memiliki peluang mendapatkan customer yang tepat"
                    </div>
                </div>
            </div>
            <div class="col-md-8 py-5 border">
                <h4 class="pb-4">Isikan detail iklanmu disini</h4>
                <form action="<?= site_url('proses_regis'); ?>" method="post" id="sign_up">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <input id="Nama Perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" class="form-control" type="text" required>
                        </div>
                        <div class="form-group col-md-6">
                          <input type="text" class="form-control" name="bidang" id="Bidang" placeholder="Bidang" required>
                        </div>
                      </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input name="nama" id="nama" placeholder="Nama Pemilik" class="form-control"  type="text" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input name="no_hp" id="no_hp" placeholder="Nomer Handphone / No WA" class="form-control"  type="text" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input  name="username" id="username" placeholder="Username" class="form-control"  type="text" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input name="email" id="email" placeholder="Email" class="form-control"  type="text" required>
                        </div>
                        <div class="form-group col-md-12">
                            <input id="Password" name="password" id="password" placeholder="Password" class="form-control"  type="password" required>
                        </div>
                        <div class="form-group col-md-12">
                            <input  name="c_password" id="c_password" placeholder="Konfirmasi Password" class="form-control"  type="password" required>
                        </div>
                        <div class="form-group col-md-12">
                            <textarea id="alamat" name="alamat"  cols="40" rows="5" class="form-control" placeholder="Alamat" required></textarea>
                        </div>
                    </div> 
                    
                    <div class="form-row">
                        <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                    
                </form>
                <br>
                <?php if($this->session->flashdata('berhasil_kirim')){ ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Berhasil</strong> Silahkan cek email anda.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?= base_url('assets/') ?>jquery-validation/jquery.validate.js"></script>
<script>
    var url_cek_email = "<?= base_url('cek_email'); ?>";
    var url_cek_no_hp = "<?= base_url('cek_no_hp'); ?>";
    var url_cek_username = "<?= base_url('cek_username'); ?>";
</script>
<script src="<?= base_url('src/frontend/') ?>js/validasi.js"></script>
</body>
</html>