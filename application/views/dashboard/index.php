<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <?php
        if ($this->session->userdata('role')=='Admin') { ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Panduan Admin</h4>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Panduan</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Fitur</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Customer Services</span></a> </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tabcontent-border">
                            <div class="tab-pane active" id="home" role="tabpanel">
                                <div class="p-20">
                                <h6>Dashboard :</h6><br>
                                    <ul>
                                        <li>Menampilkan Jumlah beberapa Data</li>
                                        <li>Menampilkan Panduan penggunaan Aplikasi</li>
                                    </ul>
                                    <br>
                                    <h6>Maping:</h6><br>
                                    <ul>
                                        <li>Polices Station : Menampilkan Marker Pos Polisi</li>
                                        <li>Red Zone : Menampilkan Daerah Rawan Bahaya</li>
                                    </ul>
                                    <br>
                                    <h6>Rivest Monitoring:</h6><br>
                                    <ul>
                                        <li>Monitoring Kecelakaan Rivest User</li>
                                    </ul>
                                    <br>
                                    <h6>Accidents:</h6><br>
                                    <ul>
                                        <li>E-Bap</li>
                                    </ul> 
                                </div>
                            </div>
                            <div class="tab-pane  p-20" id="profile" role="tabpanel">
                                <ul>
                                    <li>Dashboard</li>
                                    <li>Maping<li>
                                    <li>Rivest Monitoring<li>
                                    <li>Accidents<li>
                                </ul>
                            </div>
                            <div class="tab-pane p-20" id="messages" role="tabpanel">
                                Rivest Admin
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } 
        
        if ($this->session->userdata('role')=='Police') { ?>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Panduan Polisi</h4>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#pol" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Panduan</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#li" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Fitur</span></a> </li>
                            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#si" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Customer Service</span></a> </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content tabcontent-border">
                            <div class="tab-pane active" id="pol" role="tabpanel">
                                <div class="p-20">
                                    <h6>Dashboard :</h6><br>
                                    <ul>
                                        <li>Menampilkan Jumlah beberapa Data</li>
                                        <li>Menampilkan Panduan penggunaan Aplikasi</li>
                                    </ul>
                                    <br>
                                    <h6>Maping:</h6><br>
                                    <ul>
                                        <li>Polices Station : Menampilkan Marker Pos Polisi</li>
                                        <li>Red Zone : Menampilkan Daerah Rawan Bahaya</li>
                                    </ul>
                                    <br>
                                    <h6>Rivest Monitoring:</h6><br>
                                    <ul>
                                        <li>Monitoring Kecelakaan Rivest User</li>
                                    </ul>
                                    <br>
                                    <h6>Accidents:</h6><br>
                                    <ul>
                                        <li>E-Bap</li>
                                    </ul>      
                                </div>
                            </div>
                            <div class="tab-pane  p-20" id="li" role="tabpanel">
                                <ul>
                                    <li>Dashboard</li>
                                    <li>Maping<li>
                                    <li>Rivest Monitoring<li>
                                    <li>Accidents<li>
                                </ul>        
                            </div>
                            <div class="tab-pane p-20" id="si" role="tabpanel">
                                Rivest Admin
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } 
        
        if($this->session->userdata('role') == 'JasaRaharja')
        
        { ?>
            
           <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Panduan Jasaraharja</h4>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#jasa" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Panduan</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#rahar" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Fitur</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#ja" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Customer Services</span></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="jasa" role="tabpanel">
                            <div class="p-20">
                            <h6>Dashboard :</h6><br>
                                <ul>
                                    <li>Menampilkan Jumlah beberapa Data</li>
                                    <li>Menampilkan Panduan penggunaan Aplikasi</li>
                                </ul>
                                <br>
                                <h6>Maping:</h6><br>
                                <ul>
                                    <li>Polices Station : Menampilkan Marker Pos Polisi</li>
                                    <li>Red Zone : Menampilkan Daerah Rawan Bahaya</li>
                                </ul>
                                <br>
                                <h6>Rivest Monitoring:</h6><br>
                                <ul>
                                    <li>Monitoring Kecelakaan Rivest User</li>
                                </ul>
                                <br>
                                <h6>Accidents:</h6><br>
                                <ul>
                                    <li>E-Bap</li>
                                </ul> 
                            </div>
                        </div>
                        <div class="tab-pane  p-20" id="rahar" role="tabpanel">
                            <ul>
                                <li>Dashboard</li>
                                <li>Maping<li>
                                <li>Rivest Monitoring<li>
                                <li>Accidents<li>
                            </ul>
                        </div>
                        <div class="tab-pane p-20" id="ja" role="tabpanel">
                        Rivest Admin
                        </div>
                    </div>
                </div>
            </div>
            </div>

        <?php } 
        
        if(($this->session->userdata('role') == 'Dishub'))
        
        { ?>
            
           <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Panduan Dishub</h4>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#jasa" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Panduan</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#rahar" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Fitur</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#ja" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Customer Services</span></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="jasa" role="tabpanel">
                            <div class="p-20">
                            <h6>Dashboard :</h6><br>
                                <ul>
                                    <li>Menampilkan Jumlah beberapa Data</li>
                                    <li>Menampilkan Panduan penggunaan Aplikasi</li>
                                </ul>
                                <br>
                                <h6>Maping:</h6><br>
                                <ul>
                                    <li>Polices Station : Menampilkan Marker Pos Polisi</li>
                                    <li>Red Zone : Menampilkan Daerah Rawan Bahaya</li>
                                </ul>
                                <br>
                                <h6>Rivest Monitoring:</h6><br>
                                <ul>
                                    <li>Monitoring Kecelakaan Rivest User</li>
                                </ul>
                                <br>
                                <!-- <h6>Accidents:</h6><br>
                                <ul>
                                    <li>E-Bap</li>
                                </ul>  -->
                            </div>
                        </div>
                        <div class="tab-pane  p-20" id="rahar" role="tabpanel">
                            <ul>
                                <li>Dashboard</li>
                                <li>Maping<li>
                                <li>Rivest Monitoring<li>
                                <!-- <li>Accidents<li> -->
                            </ul>
                        </div>
                        <div class="tab-pane p-20" id="ja" role="tabpanel">
                        <!-- Rivest Admin -->
                        Rivest Admin
                        </div>
                    </div>
                </div>
            </div>
            </div>

        <?php }

        if(($this->session->userdata('role') == 'Tennant'))
        
        { ?>

            <div class="col-md-12">
            
            <?php

                $this->load->view('error/index');
            ?>

            <?php 
                $id = $this->session->userdata('id');
                $query = $this->db->get_where('iklan', array('id_tennant' => $id));
            ?>

            <div class="bd-callout bd-callout-info" style="background-color:white !important;">
                <i class="fa fa-bullhorn" aria-hidden="true" style="color:#5bc0de;font-size:40px;"></i>
                <p style="display:inline-block;padding-left:15px;"><b><font size="4">Jumlah iklan <?= $query->num_rows() ?></font></b></p>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Panduan Tenant</h4>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#jasa" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Panduan</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#rahar" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Fitur</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#ja" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Customer Services</span></a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active" id="jasa" role="tabpanel">
                            <div class="p-20">
                            <h6>Dashboard :</h6><br>
                                <ul>
                                    <li>Menampilkan dashboard untuk pemasangan iklan</li>
                                </ul>
                                <br>
                                <h6>Petunjuk pemasangan:</h6><br>
                                <ul>
                                    <li>Pilih menu iklan </li>
                                    <li>Pilih submenu pasang iklan</li>
                                    <li>Geser marker pada peta untuk lokasi tempat</li>
                                </ul>
                                <br>
                                <br>
                                <!-- <h6>Accidents:</h6><br>
                                <ul>
                                    <li>E-Bap</li>
                                </ul>  -->
                            </div>
                        </div>
                        <div class="tab-pane  p-20" id="rahar" role="tabpanel">
                            <ul>
                                <li>Tambah iklan</li>
                                <!-- <li>Accidents<li> -->
                            </ul>
                        </div>
                        <div class="tab-pane p-20" id="ja" role="tabpanel">
                            <ul>
                                <li>Email : rivest.erg@gmail.com</li>
                                <li>No kontak : 082116871007</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </div>

        <?php } ?>



        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
   
</div>

<!-- End Page wrapper  -->