<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('src/backend/'); ?>images/favicon.ico">
    <title>Login | Rivest</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('src/backend/'); ?>css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('src/backend/'); ?>css/helper.css" rel="stylesheet">
    <link href="<?php echo base_url('src/backend/'); ?>css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">

        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card" style="box-shadow:1px 2px 3px #b3e0ff;">
                            
                            <div class="login-form">
                            
                            <?php if($this->session->flashdata('berhasil')){ ?>
                                <div class="alert alert-success alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Selamat!</strong> <?= $this->session->flashdata('berhasil'); ?>.
                                </div>
                            <?php } ?>

                            <br>

                            <div class="login-head" style="align:center">
                                <img src="<?php echo base_url('src/logo/'); ?>RivestLogo.png" alt="homepage" class="dark-logo" style="height:20%; width:20%; margin-bottom:15px;" />
                                <img src="<?php echo base_url('src/logo/'); ?>RivestHeader@4x.png" alt="homepage" class="dark-logo center" style="height:20%; width:50%; margin-bottom:15px;" />
                            </div>
                            
                                <form action="<?= site_url('rivest-login'); ?>" method="post" accept-charset="utf-8">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" id="Username" aria-describedby="UsernameHelp" placeholder="Username" name="username" autocomplete="off">
                                        <small id="UsernameHelp" class="form-text text-muted">Silahkan masukan username anda</small>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" placeholder="Password" name="password" class="form-control" autocomplete="off" required/>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-outline-info m-b-30 m-t-30">Login</button>
                                    <!-- <div class="register-link m-t-15 text-center">
                                        <p>Don't have account ? <a href="#"> Sign Up Here</a></p>
                                    </div> -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('src/backend/'); ?>js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('src/backend/'); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('src/backend/'); ?>js/custom.min.js"></script>

</body>

</html>