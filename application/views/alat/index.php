
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary"><?= $s1 ?></h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active"><?= $s1 ?></li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->

    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= $s1 ?></h4>
                        <h6 class="card-subtitle">List of <?= $s1 ?></h6>
                        <div class="table-responsive m-t-40">
                        <table class="table table-bordered table-striped table-condensed flip-content" id="example">
                            <thead class="flip-content">
                                <tr>
                                    <td>No</td>
                                    <td>SSID</td>
                                    <td>Status</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>No</td>
                                    <td>SSID</td>
                                    <td>Status</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add <?= $s1 ?></h4>
                        <h6 class="card-subtitle">Form <?= $s1 ?></h6>
                        <div class="table-responsive m-t-40">
                            <form id="tryitForm" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 control-label hidden-xs">SSID</label>
                                    <div class="col-md-8 col-xs-12 colsm-12">
                                        <input type="text" class="form-control" name="ssid" id="ssid" placeholder="SSID ex eepc32xxxxx" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="status" id="status" placeholder="Sudah/Belum Aktiv" required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-3 col-md-8">
                                        <button type="submit" id="tambah_alat" class="btn btn-info">Add</button>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
        <!-- Modal -->  
        <div class="row">
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Form <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                    <form role="form" id="form-validation1" action="#" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="ssid_m" id="ssid_m" class="form-control input-md" placeholder="SSID Name" tabindex="1" data-error="SSID must be entered" required></div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="status_m" id="status_m" class="form-control input-md" placeholder="Sudah/Belum Aktiv" tabindex="2" data-error="Status must be entered" required>
                                        <input type="hidden" name="id_m" id="id_m">
                                    </div>
                                </div>
                            </div>                                
                            
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <a href="#" class="btn btn-success" data-dismiss="modal" onclick="actionEdit()">Update</a>
                    </div>

                    </div>
                </div>
            </div>

            <div class="modal" id="myModal2">
                <div class="modal-dialog">
                    <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Delete <?= $s1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form role="form" id="form-validation1" action="#" method="post">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <h2> Are You Sure Want To Delete This Data? </h2>
                                        <input type="hidden" id="id_d" name="id_d">
                                    </div>
                                </div>
                            </div>                                
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <a href="#" class="btn btn-success" data-dismiss="modal" onclick="actionDel()">Yes</a>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Container fluid  -->
    
</div>
<!-- End Page wrapper  -->
<!-- footer -->

<!-- End footer -->
        