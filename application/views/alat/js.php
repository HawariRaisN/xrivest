
    <script src="<?php echo base_url('src/backend/'); ?>js/lib/datatables/datatables.min.js"></script>

   
    <script src="https://www.gstatic.com/firebasejs/4.12.0/firebase.js"></script>
    <script>

        
    // $('.exit').click(function(){
    //     document.getElementById('myModal').setAttribute.('aria-hidden', 'true');
    //     $('#myModal').removeClass('show');
    //     //alert('ok');
    // });
   
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyB4Ddoz4G4P9KnjLrSI8XF5R0vqm3M4qB0",
        authDomain: "rivest-4507.firebaseapp.com",
        databaseURL: "https://rivest-4507.firebaseio.com",
        projectId: "rivest-4507",
        storageBucket: "rivest-4507.appspot.com",
        messagingSenderId: "1062631027"
    };
    firebase.initializeApp(config);

    var db = firebase.database();
    var alatRef = db.ref('alat');
    // var alatRefWhere = db.ref('alat')
    // ---show data----
    alatRef.on('value', showData, showError);
    function showData(data) {
        var _table = document.getElementById('myTable');
        var _tambah = '';
        var no = 1;

        data.forEach(function(child){
            _tambah += "<tr>" 
                            +"<td>" + no + "</td>" 
                            +"<td>" + child.val().ssid + "</td>" 
                            +"<td>" + child.val().status + "</td>" 
                            +"<td>"
                            +"<a href='#' onclick='editAlat(\"" +child.key+ "\")' data-toggle='modal' data-target='#myModal'><i class='fa fa-pencil-square-o'></i></a> </td>"
                            +"<td><a href='#' onclick='delAlat(\"" +child.key+ "\")'  data-toggle='modal' data-target='#myModal2'><i class='fa fa-trash-o'></i></a>"
                            +"</td>" 
                        +"</tr>";
            no++;
        })
        _table.innerHTML=_tambah;
        $('#example').DataTable();  
    }

    function showError(err) {
        console.log(err.val())
    }
    // ---show data----

    // ---Tambah Data---
    
    $("#tambah_alat").click(function(){
        var ssid = $("#ssid");
        var sts = $("#status");
        if ((ssid.val() =='') || (sts.val()=='')) {
            $(function () {
                var data = '<?= "Failed to Add Data Success" ?>';
                toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "rtl": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 1000,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                }
                toastr.error(data);
            });
        } else {
            
            alatRef.push({
            ssid : ssid.val(),
            status : sts.val()
            });

            ssid.val(''); 
            sts.val('');

            $(function () {
            var data = '<?= "Add Data Success" ?>';
                toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "rtl": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": 300,
                "hideDuration": 1000,
                "timeOut": 5000,
                "extendedTimeOut": 1000,
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                }
                toastr.success(data);
            });

        }

    });


    function editAlat(id) {
        alatRef.child(id).once('value', function(datanya){
            $("#ssid_m").val(datanya.val().ssid)
            $("#status_m").val(datanya.val().status)
            $("#id_m").val(id)
        })
    }

    function actionEdit() {
        var id = $("#id_m").val()
        alatRef.child(id).update({
            ssid : $("#ssid_m").val(),
            status : $("#status_m").val()
        })
        
        $("#ssid_m").val('')
        status : $("#status_m").val('')

         $(function () {
        var data = '<?= "Edit Data Success" ?>';
         toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "rtl": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": 300,
          "hideDuration": 1000,
          "timeOut": 5000,
          "extendedTimeOut": 1000,
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        toastr.success(data);
      });
    }

    function delAlat(id) {
        alatRef.child(id).once('value', function(datanya){
            $("#id_d").val(id)
            // console.log(id)
        })
    }

     function actionDel() {
        var id = $("#id_d").val();
        console.log(id)
        alatRef.child(id).remove()

         $(function () {
        var data = '<?= "Delete Data Success" ?>';
         toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "rtl": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": 300,
          "hideDuration": 1000,
          "timeOut": 5000,
          "extendedTimeOut": 1000,
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        toastr.success(data);
      });
    }

    </script>

</body>
</html>