    <?php if($this->session->userdata('role') == "Tennant"){ ?>
        
        <?php 
            $tanggal_sekarang = date('Y-m-d');
            $id = $this->session->userdata('id');
            $query = $this->db->get_where('des_tenant', array('id_tenant' => $id))->row();

            $tanggal_expaired = $query->expaired;
            $status_akun = $query->status_tenant;

            $start = strtotime($tanggal_sekarang);
            $end = strtotime($tanggal_expaired);

            $diff   = $end - $start;

            $bulan = floor($diff / (60 * 60 * 24 * 30));
            $hari = floor($diff / (60 * 60 * 24));

            $class = $hari <= 10 ? 'danger' : 'info';
            $icon =  $hari <= 10 ? '<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#d9534f;font-size:40px;"></i>' : '<i class="fa fa-info" aria-hidden="true" style="color:#5bc0de;font-size:40px;"></i>';
        ?>

        <?php if($status_akun == 'trailer'){ ?>
            <div class="bd-callout bd-callout-<?= $class ?>" style="background-color:white !important;">
                <?= $icon ?>
                <p style="display:inline-block;padding-left:15px;"><b><font size="4">Status akun <?= $status_akun == 'trailer' ? "<font color='red'>Trial</font>" : '<font color="green">premium</font>'?></font></b> <br /> <font size="2">waktu tersisa <b><?= $bulan > 0 ? $bulan : '0'; ?></b> bulan  <b><?= $hari > 0 ? $hari : '0';  ?></b> hari </font></p>
                
            </div>
        <?php } ?>

    <?php } ?>


