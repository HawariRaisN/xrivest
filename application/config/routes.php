<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['default_controller'] = 'C_Frontend';

//Dashboard
$route['dashboard'] = 'Dashboard/index';
$route['index'] = 'C_Frontend/index';

//Alat = rd
$route['rd'] = 'Alat/index';
$route['rd/test'] = 'Alat/test';


//Pengguna Super User = su
$route['su'] = 'Su/index';
$route['su/a'] = 'Su/add';
// $route['su/up/(:any)']= 'Su/edit_su/(:any)';
// $route['su/au']= 'Su/proses_edit_su';
$route['su/del/(:any)']= 'Su/delete_su/(:any)';
//Pengguna Rivest = r
$route['r'] = 'Pengguna/index';


//Kecelakaan
$route['k'] = 'Kecelakaan/index';
$route['k/jr'] = 'Kecelakaan/accident';
$route['k/dcs'] = 'Kecelakaan/decission';
$route['k/jsrhrj'] = 'Kecelakaan/jsrhrj';
$route['k/dijkstra'] = 'Kecelakaan/rute';

//Login
$route['rivest-login'] = 'Auth';
$route['logout']= 'Auth/logout';

//Police Station = ps
$route['ps']= 'Pos/index';
$route['ps/a']= 'Pos/add';
$route['ps/ac']= 'Pos/action_add';
$route['ps/up/(:any)']= 'Pos/update/(:any)';
$route['ps/del/(:any)']= 'Pos/delete_pos/(:any)';
$route['ps/au']= 'Pos/action_update';

//Red Zone = rs
$route['rz']= 'Rawan/index';
$route['rz/az']= 'Rawan/add';
$route['rz/a']= 'Rawan/action_add';
$route['rz/up/(:any)']= 'Rawan/edit_rawan/(:any)';
$route['rz/au']= 'Rawan/proses_edit_rawan';
$route['rz/del/(:any)']= 'Rawan/delete_rawan/(:any)';


//Android = an
$route['and/get']= 'Android/getRawan';
$route['and/count']= 'Android/hitung';
$route['and/getlokasi']= 'Android/getlokasi';
$route['and/start']= 'Android/setRide';
$route['and/end']= 'Android/endRide';
$route['and/getnotif']= 'Android/area_notif';
$route['and/tennant']= 'Android/getTennant';
$route['and/berita'] = 'C_Berita/getBerita';
$route['and/detail_berita/(:any)'] = 'C_Berita/detailBeritaFrontend/$1';
$route['and/settoken']= 'Android/allow_notif_police';
$route['and/getpolicenotif']= 'Android/police_notif';

//Registrasi Tenant
$route['regis_tenant'] = 'C_Frontend/regis_tenant';
$route['panggil'] = 'C_Frontend/panggil';
$route['proses_regis'] = 'C_Frontend/store_registrasi';
$route['regis/verification/(:any)'] = 'C_Frontend/verification/$1';
$route['regis']= 'C_Tenant/index';
$route['regis_post']= 'C_Tenant/regis_post';
$route['regis_store_tenant']= 'C_Tenant/regis_store';
$route['formAdd']= 'C_Tenant/formAdd';
$route['tenant/up/(:any)']= 'C_Tenant/update/$1';
$route['tenant/del/(:any)']= 'C_Tenant/delete_tenant/$1';
$route['tenant/au']= 'C_Tenant/action_update';
$route['cek_email'] = 'C_Frontend/cekEmail';
$route['cek_no_hp'] = 'C_Frontend/cekNoHp';
$route['cek_username'] = 'C_Frontend/cekUsername';
// $route['upload_regis'] = 'Sendmail/index';

//Route Berita
$route['tampil_berita'] = 'C_Berita/index';
$route['tambah_berita'] = 'C_Berita/addBerita';
$route['proses_tambah_berita'] = 'C_Berita/prosesAdd';
$route['update_berita/(:any)'] = 'C_Berita/updateBerita/$1';
$route['proses_update'] = 'C_Berita/action_update';
$route['delete_berita/(:any)'] = 'C_Berita/hapusBerita/$1';
$route['detail_berita/(:any)'] = 'C_Berita/detailBerita/$1';

$route['get_data_pdf/(:any)'] = 'Laporanpdf/index/$1';

//Settingan 
$route['(:any)'] = 'errors/show_404';
$route['(:any)/(:any)'] = 'errors/show_404';
$route['(:any)/(:any)/(:any)'] = 'errors/show_404';